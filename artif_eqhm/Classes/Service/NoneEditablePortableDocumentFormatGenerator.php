<?php
namespace Artif\ArtifEqhm\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Markus Döll
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Artif\ArtifEqhm\Domain\Model\Practice;
use InvalidArgumentException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Exception;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use mikehaertl\pdftk\Pdf;


require_once(GeneralUtility::getFileAbsFileName('typo3conf/ext/artif_eqhm/Libraries/autoload.php'));


class NoneEditablePortableDocumentFormatGenerator extends AbstractService
{

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile
     * @return string
     * @throws Exception
     */
	public function generatePDF(\Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile)
	{
//		$outputFilePath = \Artif\ArtifEqhm\Utility\NameGenerator::generateFileName('pdf', $this->practice->getUid());
        $outputFilePath = \Artif\ArtifEqhm\Utility\NameGenerator::getFileNamePath('pdf', $documentFile);

		/** @var \TYPO3\CMS\Extbase\Domain\Model\FileReference $file */
		$file = $documentFile->getFile();
		if ($file){
            /** Get Fields for Mapping */
            $pdf = new Pdf($file->getOriginalResource()->getPublicUrl());
        } else {
		    throw new Exception("Selected DocumentFile UID:". $documentFile->getUid() ." has no file assigned.");
        }

//		DebuggerUtility::var_dump($pdf);die;
        $pdf->saveAs($outputFilePath);
		if (!$pdf->saveAs($outputFilePath)) {
			$error = $pdf->getError();
			return $error;
		} else {
			return $outputFilePath;
		}
	}
}