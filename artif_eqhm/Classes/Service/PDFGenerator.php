<?php
namespace Artif\ArtifEqhm\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Markus Döll
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use mikehaertl\pdftk\Pdf;


require_once(GeneralUtility::getFileAbsFileName('typo3conf/ext/artif_eqhm/Libraries/autoload.php'));


class PDFGenerator
{

	/**
	 * @var \Artif\ArtifEqhm\Domain\Model\Practice
	 */
	protected $practice;

	public function initService(Practice $practice)
	{
		$this->practice = $practice;

	}

	/**
	 * @param \Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile
	 *
	 * @return string
	 */
	public function generatePDF(\Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile)
	{

		$outputFilePath = 'typo3temp/temp/test.pdf';
		$error = 'test';

		/** @var \TYPO3\CMS\Extbase\Domain\Model\FileReference $file */
		$file = $documentFile->getFile();

		/** Get Fields for Mapping */
		$pdf = new Pdf($file->getOriginalResource()->getPublicUrl());
		$filledFields = $this->generateFieldMapping($pdf->getDataFields());

		/** Generate new clean PDF Instance */
		$pdf = new Pdf($file->getOriginalResource()->getPublicUrl());
		$pdf->fillForm($filledFields)->needAppearances()->flatten();

		if (!$pdf->saveAs($outputFilePath)) {
			$error = $pdf->getError();
			return $error;
		} else {
			return $outputFilePath;
		}
	}

	/**
	 * Generate Field Mapping from PDF Fieldnames
	 *
	 * @param string $fieldNames
	 *
	 * @return array
	 */
	protected function generateFieldMapping($fieldNames)
	{
		$fieldMapping = array();
		preg_match_all("/FieldName: ([a-z0-9;,_.]*)/", $fieldNames, $matchedFieldNames);

		foreach ($matchedFieldNames[1] as $onePdfField) {
			if (preg_match('/[.]/', $onePdfField)) {
				$splittedField = explode('.', $onePdfField);
				$fieldMapping[$onePdfField] = $this->{$splittedField[0]}->{'get' . ucfirst($splittedField[1])}();


			}
		}
		return $fieldMapping;
	}


}