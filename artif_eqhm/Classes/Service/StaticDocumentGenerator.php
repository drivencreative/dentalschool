<?php
/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 10/17/17
 * Time: 10:21 AM
 */

namespace Artif\ArtifEqhm\Service;


use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Generators\FactoryDocumentGenerator;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class StaticDocumentGenerator extends DocumentManager
{


    /**
     * @var \Artif\ArtifEqhm\Generators\FactoryDocumentGenerator
     * @inject
     */
    protected $factoryDocumentGenerator = null;

    /**
     * @var \Artif\ArtifEqhm\Service\DocumentManager
     * @inject
     */
    protected $documentManager = null;


    /**
     * @param Practice $practice
     */
    public function generateStaticDocuments(Practice $practice): void
    {

//        if (!$practice->getHygieneplan()) {
//            $this->factoryDocumentGenerator->generate('HygienePlan', $practice);
//        }
        $this->factoryDocumentGenerator->generate('HygienePlan', $practice);

//        if (!$practice->getOrganigramm()) {
//            $this->factoryDocumentGenerator->generate('Organigramm', $practice);
//        }
        $this->factoryDocumentGenerator->generate('Organigramm',$practice);

//        if (!$practice->getQmHandbuch()) {
//            $this->factoryDocumentGenerator->generate('QMHandbuch',$practice, $this->factoryDocumentGenerator->generate('Organigramm', $practice));
//        }
        $this->factoryDocumentGenerator->generate('QMHandbuch',$practice, $this->factoryDocumentGenerator->generate('Organigramm', $practice));
    }

}