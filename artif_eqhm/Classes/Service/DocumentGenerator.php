<?php

namespace Artif\ArtifEqhm\Service;


use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Utility\CssToInline;
use mikehaertl\pdftk\Pdf;
use mikehaertl\wkhtmlto\Pdf as WkPdf;
use TYPO3\CMS\Core\Log\Exception;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Fluid\View\StandaloneView;

class DocumentGenerator extends AbstractService
{
    /**
     * @param array $docsToMerge
     * @param $relativeFilePath
     * @throws \HttpException
     */
    public function append(array $docsToMerge, $relativeFilePath)
    {
        /** @var Pdf $pdf Get Hygieneplan pdf file */
        $pdf = new Pdf($docsToMerge);
        /** Fill data into file and save new pdf */
        if (!$pdf->saveAs(PATH_site . $this->storage->getConfiguration()['basePath'] . $relativeFilePath)) {
            $error = $pdf->getError();
        }
        return $relativeFilePath;

    }

    /**
     * @param Practice $practice
     * @param DocumentFile|null $documentFile
     * @param $template
     * @return string
     */
    public function generateView(Practice $practice=null, DocumentFile $documentFile = null, $template)
    {
        return $this->getStandaloneView(
            [
                'practice' => $practice,
                'documentFile' => $documentFile
            ],
            $template
        );
    }

    /**
     * render fluid standalone view
     *
     * @param array $variables objects passed to view
     * @param string $template file name of partial template
     *
     * @param null $css
     * @return string
     */
    public function getStandaloneView($variables, $template, $css = NULL)
    {
        $templatePathAndFilename = ExtensionManagementUtility::extPath('artif_eqhm') . 'Resources/Private/' . $template;
        $standaloneView = GeneralUtility::makeInstance(StandaloneView::class);
        $standaloneView->setFormat('html');
        $standaloneView->setTemplatePathAndFilename($templatePathAndFilename);
        $standaloneView->assignMultiple($variables);
        $data = $standaloneView->render();

        $data = self::cssToInline($data, $css);
        return $data;
    }


    /**
     * Css to Inline
     *
     * @param string $html
     * @param string $css
     *
     * @return string
     **/
    public static function cssToInline($html, $css = '') {
        if ($css) {
            $cssToInline = new CssToInline($html, file_get_contents($css));
            $html = $cssToInline->emogrify();
        }
        return $html;
    }

    /**
     * @param $name
     * @param $content
     * @return string
     * @throws \HttpException
     * @internal param Practice $practice
     * @internal param $pdf
     */
    public function htmlToPdf($name, $content, $options = []): string
    {
        try {
            /** @var WkPdf $pdf Get Hygieneplan template with fields */
            $pdf = $this->objectManager->get(WkPdf::class,
                array_merge(
                    $options,
                    [
                        'no-outline',
                        'encoding' => 'UTF-8',

                        'margin-top' => 10,
                        'margin-right' => 0,
                        'margin-bottom' => 10,
                        'margin-left' => 0,

                        // Default page options
                        'disable-smart-shrinking',
                        'user-style-sheet' => ExtensionManagementUtility::extPath('artif_eqhm') . $this->settings['pdf.']['templateCss'],
                    ]
                )
            );
            $resultFilePath = 'user_upload/' .$name . '.pdf';
            $pdf->addPage($content);
//            DebuggerUtility::var_dump($this->storage->getDefaultFolder());die;
            $pdf->saveAs(PATH_site . $this->storage->getConfiguration()['basePath'] . $resultFilePath);
            return $resultFilePath;
        } catch (\Exception $e) {
            throw new \HttpException('Could not stamp or save PDF: ' . $pdf->getError());
        }
    }

}