<?php
/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 10/9/17
 * Time: 10:13 AM
 */

namespace Artif\ArtifEqhm\Service;

use Artif\ArtifEqhm\Domain\Model\Document;
use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Domain\TypeConverter\FileReferenceConverter;
use Artif\ArtifEqhm\Utility\IteratorHandler;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Resource\ResourceInterface;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidArgumentValueException;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class DocumentManager extends AbstractService
{

    /**
     * DocumentRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentRepository
     * @inject
     */
    protected $documentRepository = null;

    /**
     * DocumentFileRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentFileRepository
     * @inject
     */
    protected $documentFileRepository = null;

    /**
     * PracticeRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\PracticeRepository
     * @inject
     */
    protected $practiceRepository = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * @var \Artif\ArtifEqhm\Service\DocumentGenerator
     * @inject
     */
    protected $documentGenerator = null;

    /**
     * CategoryRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @param Document $document
     * @param Practice $practice
     * @param bool $version
     * @return array
     */
    public function createNewCloneDocument(Document $document, Practice $practice, $version = false): array
    {
        $documentFile = $document->getUid() ? $documentFile = $this->copyDocumentFile($document->getNewestVersionOfDocumentFile()) : $this->objectManager->get(DocumentFile::class);
        if ($version) $documentFile->setVersion(($documentFile->getVersion() ?: 0) + 1);
        $this->unsignedDocumentFile($documentFile);
        $practiceDocument = $document;
        if ($document->getPractice()) {
            if ($document->getPractice()->getUid() != $practice->getUid()) {
                $practiceDocument = $this->copyDocument($document, $practice);
            }
        }
        $practiceDocument->addDocumentFile($documentFile);

        $practiceDocument->setPid($this->settings['storagePid']);
        $this->documentRepository->add($practiceDocument);
        $this->persistenceManager->persistAll();
        return array ($documentFile, $practiceDocument);
    }

    /**
     * @param DocumentFile $newestVersionOfDocumentFile
     */
    private function unsignedDocumentFile(DocumentFile $newestVersionOfDocumentFile): void
    {
        $newestVersionOfDocumentFile->setSigned(null);
        $newestVersionOfDocumentFile->setSignatureImage(null);
        $newestVersionOfDocumentFile->setSignatureName('');
    }

    /**
     * Copy document
     *
     * @param Document $document
     * @param Practice $practice
     * @return Document $document
     */
    public function copyDocument(Document $document, Practice $practice)
    {
        /** @var Document $newDocument */
        $newDocument = $this->objectManager->get(Document::class);
        $newDocument->setPractice($practice);
        $newDocument->setName($document->getName());
        $newDocument->setTyp($document->getTyp());
        $newDocument->setName($document->getName());
        $newDocument->setBasedOn($document->getBasedOn());
        $newDocument->setBasedOnDocumentVersion($document->getNewestVersionOfDocumentFile()->getVersion());
        $newDocument->setCloneOf($document);
        $newDocument->setPid($document->getPid());

        foreach ($document->getDocumentFiles()->toArray() as $file) {
            if ($file instanceof DocumentFile) {
                $newFile = $this->copyDocumentFile($file);
                $newDocument->addDocumentFile($newFile);
            }
        }
        foreach ($document->getCategories() as $category) {
            if ($category instanceof Category) {
                $newDocument->addCategory($category);
            }
        }
        return $newDocument;
    }

    /**
     * Copy DocumentFiles
     *
     * @param $documentFile
     * @return DocumentFile $newDocumentFile
     */
    public function copyDocumentFile(DocumentFile $documentFile)
    {
        /** @var DocumentFile $newDocumentFile */
        $newDocumentFile = $this->objectManager->get(DocumentFile::class);

        $newDocumentFile->setContent($documentFile->getContent());
        $newDocumentFile->setAdditionalContent($documentFile->getAdditionalContent());
        $newDocumentFile->setVersion($documentFile->getVersion());
        $newDocumentFile->setSigned($documentFile->getSigned());
        $newDocumentFile->setSignatureImage($documentFile->getSignatureImage());
        $newDocumentFile->setSignatureName($documentFile->getSignatureName());
        $newDocumentFile->setPending($documentFile->getPending());
        $newDocumentFile->setPid($documentFile->getPid());

        if ($documentFile->getFile()) {
            $newDocumentFile->setFile($this->objectManager->get(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class));
            $newDocumentFile->getFile()->setOriginalResource($documentFile->getFile()->getOriginalResource());
        }
        return $newDocumentFile;
    }

    /**
     * @param Practice $practice
     * @param ResourceInterface $fileRef
     */
    public function updatePracticeDocumentFile(Practice $practice, ResourceInterface $fileRef, Document $currentDocument): void
    {
        if (pathinfo($fileRef->getName())['filename']) {
            $currentDocument->setName(pathinfo($fileRef->getName())['filename']);
        }

        /** @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile> $documentFiles */
        $documentFiles = $currentDocument->getDocumentFiles();
        if (empty($documentFiles->toArray()[0]->getFile())) {
            /** @var FileReference $file */
            $documentFiles
                ->toArray()[0]
                ->setFile($this->objectManager->get(FileReference::class));
            $this->documentFileRepository->update($documentFiles->toArray()[0]);
            $this->persistenceManager->persistAll();
        }
        /** @var FileReference $file */
        $file = $documentFiles
            ->toArray()[0]
            ->getFile();
        $file->setOriginalResource($fileRef);
        $this->practiceRepository->update($practice);
        $this->persistenceManager->persistAll();
    }

    /**
     * @param Practice $practice
     * @param $fileRef
     * @return Document
     */
    public function createNewPracticeDocumentFile(Practice $practice, $fileRef): Document
    {
        /** @var Document $document Add Document to practice */
        /** @var DocumentFile $documentFile */
        list($documentFile, $document) = $this->createNewCloneDocument($this->objectManager->get(Document::class), $practice);
        $file = $this->objectManager->get(FileReference::class);
        $documentFile->setFile($file);
        $documentFile
            ->getFile()
            ->setOriginalResource($fileRef);
        $this->documentFileRepository->update($documentFile);
        if (pathinfo($fileRef->getName())['filename']) {
            $document->setName(pathinfo($fileRef->getName())['filename']);
        }
        $document->setBasedOnDocumentVersion('0');
        $document->setPractice($practice);
        return $document;
    }

    /**
     * @param Practice $practice
     * @param $filePaths
     * @internal param string $filePath
     * @internal param null $content
     */
    public function createGenericNonEditableDocument(Practice $practice, $filePaths): void
    {

        if (!empty($filePaths)) {

            if ($filePaths['organigramm']) {
                $fileRef = $this->generateDocumentFromFile($practice, $filePaths['organigramm']);
                if ($practice->getOrganigramm()) {
                    $document = $practice->getOrganigramm();
                    $documentFile = $practice->getOrganigramm()->getDocumentFiles()->toArray()[0];
                    $documentFile->getFile()->setOriginalResource($fileRef);
                } else {
                    $document = $this->createNewPracticeDocumentFile($practice, $fileRef);
                    $document->setTyp('NoneEditablePortableDocumentFormat');
                    $document->setGeneric(true);
                }
                if ($this->settings['categories.']['organigramm']) {
                    /** @var Category $category */
                    $category = $this->categoryRepository->findByUid($this->settings['categories.']['organigramm']);
                    $document->addCategory($category);
                }
                $practice->setOrganigramm($document);
                $this->practiceRepository->update($practice);
                $this->updatePracticeDocumentFile($practice, $fileRef, $document);
            }

            if ($filePaths['QMHandbuch']) {
                $fileRef = $this->generateDocumentFromFile($practice, $filePaths['QMHandbuch']);
                if ($practice->getQmHandbuch()) {
                    $document = $practice->getQmHandbuch();
                    $documentFile = $practice->getQmHandbuch()->getDocumentFiles()->toArray()[0];
                    $documentFile->getFile()->setOriginalResource($fileRef);
                } else {
                    $document = $this->createNewPracticeDocumentFile($practice, $fileRef);
                    $document->setTyp('NoneEditablePortableDocumentFormat');
                    $document->setGeneric(true);
                }
                if ($this->settings['categories.']['QMHandbuch']) {
                    /** @var Category $category */
                    $category = $this->categoryRepository->findByUid($this->settings['categories.']['QMHandbuch']);
                    $document->addCategory($category);
                }
                $practice->setQmHandbuch($document);
                $this->practiceRepository->update($practice);
                $this->updatePracticeDocumentFile($practice, $fileRef, $document);
            }
            $this->practiceRepository->persistAll();
        }
    }

    /**
     * Upload and Create file reference object (File Abstraction Layer)
     *
     * @param string $file
     * @param string $folder
     *
     * @return null|\TYPO3\CMS\Core\Resource\FileInterface
     */
    protected function createFalFile(Practice $practice, $file, $folder)
    {
        if (!empty($file)) {
            $uploadDir = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName('fileadmin/' . $folder);
            if (!is_dir($uploadDir)) {
                \TYPO3\CMS\Core\Utility\GeneralUtility::mkdir_deep($uploadDir);
            }

            /** @var ResourceStorage $storage */
            $storage = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Resource\StorageRepository::class)->findByUid(1);
            return $storage->getFile($file);
        }

        return null;
    }

    /**
     * @param Practice $practice
     * @param $documentPath
     * @return \TYPO3\CMS\Core\Resource\FileReference
     */
    public function generateDocumentFromFile(Practice $practice, $documentPath): \TYPO3\CMS\Core\Resource\FileReference
    {
        /** @var ResourceInterface $resource */
        $resource = $this->createFalFile($practice, $documentPath, 'user_upload');

        /** @var \TYPO3\CMS\Core\Resource\ResourceFactory $resourceFactory - https://forge.typo3.org/issues/78044 * */
        $resourceFactory = $this->objectManager->get(\TYPO3\CMS\Core\Resource\ResourceFactory::class);
        $fileRef = $resourceFactory->createFileReferenceObject(['uid_local' => $resource->getUid()]);
        return $fileRef;
    }

    /**
     * @param $practice
     * @param $content
     * @param $additionalContent
     * @param $documentName
     * @return Document
     */
    public function generateDocumentFromContent($practice = null, $content = null, $documentName, $additionalContent = null)
    {
        /** @var DocumentFile $documentFile */
        $documentFile = $this->objectManager->get(DocumentFile::class);
        $documentFile->setContent($content);
        $documentFile->setAdditionalContent($additionalContent);

        /** @var Document $document */
        $document = $this->objectManager->get(Document::class);
        if ($practice) {
            $document->setPractice($practice);
        }
        $document->setName($documentName);
        $document->setTyp('Table');
        $document->setPid($this->settings['storagePid']);
        $documentFile->setDocument($document);
        $document->addDocumentFile($documentFile);

        $this->documentRepository->add($document);
        $this->documentRepository->persistAll();
        return $document;

    }

    /**
     * @param Practice $practice
     * @param $content
     * @param $additionalContent
     */
    public function createGenericHygienePlanDocument(Practice $practice, $content, $additionalContent): void
    {
        if (!$practice->getHygieneplan()) {
//            $document = $practice->getHygieneplan();
//            $documentFile = $document->getDocumentFiles()->toArray()[0];
//            $documentFile->setContent($content);
//            $documentFile->setAdditionalContent($additionalContent);

            $document = $this->generateDocumentFromContent($practice, $content, 'Hygieneplan' . $practice->getUid(), $additionalContent);
            $document->setBasedOnDocumentVersion('0');
            if ($this->settings['categories.']['hygieneplan']) {
                /** @var Category $category */
                $category = $this->categoryRepository->findByUid($this->settings['categories.']['hygieneplan']);
                $document->addCategory($category);
            }
            $document->setIsHygienePlan(true);
            $practice->setHygieneplan($document);
            $this->practiceRepository->update($practice);
        }
    }
}