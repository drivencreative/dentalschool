<?php
/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 11/21/17
 * Time: 3:17 PM
 */

namespace Artif\ArtifEqhm\Service;


use Artif\ArtifEqhm\Domain\Model\Category;
use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;

class DeviceManager extends AbstractService
{
    /**
     * DeviceRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DeviceRepository
     * @inject
     */
    protected $deviceRepository = null;

    /**
     * DocumentManager
     * @var \Artif\ArtifEqhm\Service\DocumentManager
     * @inject
     */
    protected $documentManager;

    /**
     * @var \Artif\ArtifEqhm\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository;

    /**
     * @var \Artif\ArtifEqhm\Domain\Repository\PracticeRepository
     * @inject
     */
    protected $practiceRepository;

    /**
     * CategoryRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @return \TYPO3\CMS\Fluid\View\StandaloneView
     */
    public function getView()
    {
        /** @var \TYPO3\CMS\Fluid\View\StandaloneView $standAloneView */
        $standAloneView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');

        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
        $templateRootPath = GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['templateRootPaths'][count($extbaseFrameworkConfiguration['view']['templateRootPaths']) - 1]);
        $templatePathAndFilename = $templateRootPath . 'Device/DataList.html';

        $standAloneView->setFormat('html');
        $standAloneView->setTemplatePathAndFilename($templatePathAndFilename);
        $standAloneView->setTemplateRootPaths($extbaseFrameworkConfiguration['view']['templateRootPaths']);
        $standAloneView->setLayoutRootPaths($extbaseFrameworkConfiguration['view']['layoutRootPaths']);
        $standAloneView->setPartialRootPaths($extbaseFrameworkConfiguration['view']['partialRootPaths']);

        return $standAloneView;
    }

    /**
     * generate Document
     *
     * @param Practice $practice
     * @param Category $category
     * @return \Artif\ArtifEqhm\Domain\Model\Document
     */
    public function generateDocument(Practice $practice, Category $category)
    {
        return $this->documentManager->generateDocumentFromContent(
            $practice,
            $this->generateTemplate($practice, $category),
            LocalizationUtility::translate('tx_artifeqhm_domain_model_device.document_prefix', 'ArtifEqhm') . $this->categoryRepository->findByUid($category->getUid())->getTitle()
        );
    }

    /**
     * generate Template
     *
     * @param Practice $practice
     * @param Category $category
     * @return string
     */
    public function generateTemplate(Practice $practice, Category $category)
    {
        return $this->getView()
            ->assign('devices', $this->deviceRepository->findByPracticeAndCategory($practice, $category->getUid()))
            ->assign('category', $category)
            ->render();
    }

}