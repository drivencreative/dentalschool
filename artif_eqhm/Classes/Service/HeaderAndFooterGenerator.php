<?php
namespace Artif\ArtifEqhm\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Markus Döll
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use mikehaertl\pdftk\Pdf;



class HeaderAndFooterGenerator extends AbstractService
{

	/**
	 * @param $templateFile
	 *
	 * @return \TYPO3\CMS\Fluid\View\StandaloneView
	 */
	protected function getView($templateFile)
	{
		/** @var \TYPO3\CMS\Fluid\View\StandaloneView $standAloneView */
		$standAloneView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');

		$extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(\TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$templateRootPath = \TYPO3\CMS\Core\Utility\GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['templateRootPaths'][count($extbaseFrameworkConfiguration['view']['templateRootPaths']) - 1]);
		$templatePathAndFilename = $templateRootPath . "DocumentTemplates/$templateFile.html";

		$standAloneView->setFormat('html');
		$standAloneView->setTemplatePathAndFilename($templatePathAndFilename);
		$standAloneView->setTemplateRootPaths($extbaseFrameworkConfiguration['view']['templateRootPaths']);
		$standAloneView->setLayoutRootPaths($extbaseFrameworkConfiguration['view']['layoutRootPaths']);
		$standAloneView->setPartialRootPaths($extbaseFrameworkConfiguration['view']['partialRootPaths']);

		return $standAloneView;
	}

	public function generateHeader(Practice $practice){
		$tempHeaderFile = $this->settings['tempFilePath'].'header'.$practice->getUid().'.html';

		$view = $this->getView('Header');
		$view->assign('practice',$practice);
		file_put_contents($tempHeaderFile,$view->render());

		return $tempHeaderFile;
	}

	public function generateFooter(DocumentFile $documentFile){
		$tempFooterFile = $this->settings['tempFilePath'].'/footer'.$documentFile->getUid().'.html';
		$view = $this->getView('Footer');


		$view->assign('documentFile',$documentFile);
		$view->assign('pathSite', PATH_site);
		file_put_contents($tempFooterFile,$view->render());

		return $tempFooterFile;
	}


}