<?php
namespace Artif\ArtifEqhm\Service;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2016 Markus Döll <markus.doell@artif.com>
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class AbstractService
{

	/**
	 * @var \TYPO3\CMS\Extbase\Object\ObjectManager
	 */
	protected $objectManager;
	/**
	 * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
	 */
	protected $configurationManager;
	/**
	 * @var array
	 */
	protected $settings;

	/**
	 * @var \Artif\ArtifEqhm\Domain\Model\Practice
	 */
	protected $practice;

	protected $storageUid = 1;

    /**
     * @var ResourceStorage
     */
	protected $storage;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     */
	protected $resourceFactory;


	public function __construct()
	{
	    /** @var \TYPO3\CMS\Core\Resource\ResourceFactory $resourceFactory */
        $this->resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

        /** @var ResourceStorage $storage */
        $this->storage = $this->resourceFactory->getStorageObject($this->storageUid);
		$this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
		$this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManager');

		$extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

		$this->settings = $extbaseFrameworkConfiguration['plugin.']['tx_artifeqhm_dm.']['settings.'];
	}


	public function initService(Practice $practice)
	{
		$this->practice = $practice;

	}



}