<?php

namespace Artif\ArtifEqhm\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Markus Döll
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class DataHandler extends \TYPO3\CMS\Core\DataHandling\DataHandler
{

	/**
	 * __construct
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Simulate Backend User for DataHandler
	 */
	public function simulateBackendUser()
	{
		/** @var \TYPO3\CMS\Backend\FrontendBackendUserAuthentication $BE_USER */
		$BE_USER = GeneralUtility::makeInstance('TYPO3\\CMS\\Backend\\FrontendBackendUserAuthentication');
		$BE_USER->setBeUserByName('jan.loderhose');
		if ($BE_USER->user['uid']) {
			$BE_USER->fetchGroupData();
		}
		$BE_USER->uc_default['copyLevels'] = '9999';
		$BE_USER->uc = $BE_USER->uc_default;
		$GLOBALS['PAGES_TYPES'][254]['allowedTables'] = '*';
		return $BE_USER;
	}

	/**
	 * Copying a single record
	 *
	 * @param \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $value
	 * @param boolean                                               $first Is a flag set, if the record copied is NOT a 'slave' to another record copied. That is, if this record was asked to be copied in the cmd-array
	 * @param array                                                 $overrideValues Associative array with field/value pairs to override directly. Notice; Fields must exist in the table record and NOT be among excluded fields!
	 * @param string                                                $excludeFields Commalist of fields to exclude from the copy process (might get default values)
	 * @param integer                                               $language Language ID (from sys_language table)
	 *
	 * @return integer ID of new record, if any
	 */
	public function copy($object, $first = 0, $overrideValues = array(), $excludeFields = '', $language = 0)
	{
		if ($object instanceof \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface) {
			$this->BE_USER = $this->simulateBackendUser();
			$this->userid = $this->BE_USER->user['uid'];
			$this->username = $this->BE_USER->user['username'];
			$this->admin = true;

			if (!is_object($GLOBALS['LANG'])) {
				$GLOBALS['LANG'] = GeneralUtility::makeInstance('TYPO3\CMS\Lang\LanguageService');
				$GLOBALS['LANG']->csConvObj = GeneralUtility::makeInstance('TYPO3\CMS\Core\Charset\CharsetConverter');
			}

			$newUid = $this->copyRecord($this->resolveTableName($object), $object->getUid(), $object->getPid(), 1, $overrideValues, $excludeFields, $language);
			return $newUid;
		}
	}

	/**
	 * Resolve the table name for the given class name
	 *
	 * @param \TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $domainObject
	 *
	 * @return string The table name
	 */
	public function resolveTableName(\TYPO3\CMS\Extbase\DomainObject\DomainObjectInterface $domainObject)
	{
		$className = get_class($domainObject);
		if (strpos($className, '\\') !== false) {
			$classNameParts = explode('\\', $className, 6);
			// Skip vendor and product name for core classes
			if (strpos($className, 'TYPO3\\CMS\\') === 0) {
				$classPartsToSkip = 2;
			} else {
				$classPartsToSkip = 1;
			}
			$tableName = 'tx_' . strtolower(implode('_', array_slice($classNameParts, $classPartsToSkip)));
		} else {
			$tableName = strtolower($className);
		}
		return $tableName;
	}

	/**
	 * @param DocumentFile $oldObject
	 * @param DocumentFile $newObject
	 */
	public function fixSysFileReference($oldObject,$newObject)
	{
		/** @var QueryBuilder $queryBuilder */
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_reference');

		$queryBuilder->update('sys_file_reference')
			->set('uid_foreign', $newObject->getUid())
			->where(
				$queryBuilder->expr()->eq('uid',$oldObject->getFile()->getUid())
//				$queryBuilder->expr()->eq('tablenames',$queryBuilder->createNamedParameter($this->resolveTableName($oldObject)))
//				$queryBuilder->expr()->eq('uid_foreign', $queryBuilder->createNamedParameter($oldObject->getUid()))

			);

//			DebuggerUtility::var_dump($queryBuilder->getSQL());
//			DebuggerUtility::var_dump($queryBuilder->getParameters());
			$queryBuilder->execute();
	}

	/**
	 * @param DocumentFile $object
	 */
	public function removeAllSysFileReference($object){
		/** @var QueryBuilder $queryBuilder */
		$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('sys_file_reference');

		$queryBuilder->delete('sys_file_reference')
			->where(
				$queryBuilder->expr()->eq('uid',$object->getFile()->getUid())
			)
			->execute();


//		DebuggerUtility::var_dump($queryBuilder->getSQL());
//		DebuggerUtility::var_dump($queryBuilder->getParameters());
		$queryBuilder->execute();
	}
}

?>