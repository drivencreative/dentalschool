<?php
/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 10/18/17
 * Time: 10:15 AM
 */

namespace Artif\ArtifEqhm\Service;

use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;
use TYPO3\CMS\Core\Error\Http\AbstractServerErrorException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use mikehaertl\wkhtmlto\Pdf;
use TYPO3\CMS\Install\Controller\Action\Tool\SystemEnvironment;


require_once(GeneralUtility::getFileAbsFileName('typo3conf/ext/artif_eqhm/Libraries/autoload.php'));

class HygienePlanDocumentGenerator extends TableGenerator
{
}