<?php

namespace Artif\ArtifEqhm\Service;

/***************************************************************
 * Copyright notice
 *
 * (c) 2017 Markus Döll
 *
 * All rights reserved
 *
 * This script is part of the TYPO3 project. The TYPO3 project is
 * free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * The GNU General Public License can be found at
 * http://www.gnu.org/copyleft/gpl.html.
 *
 * This script is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Core\SystemEnvironmentBuilder;
use TYPO3\CMS\Core\Error\Http\AbstractServerErrorException;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use mikehaertl\wkhtmlto\Pdf;
use TYPO3\CMS\Install\Controller\Action\Tool\SystemEnvironment;


require_once(GeneralUtility::getFileAbsFileName('typo3conf/ext/artif_eqhm/Libraries/autoload.php'));


class DeviceListGenerator extends AbstractService
{
	/**
	 * @var HeaderAndFooterGenerator
	 */
	protected $headerAndFooterGenerator;

	public function __construct()
	{
		parent::__construct();
		$this->headerAndFooterGenerator = $this->objectManager->get('Artif\ArtifEqhm\Service\HeaderAndFooterGenerator');
	}

	protected function getView()
	{
		/** @var \TYPO3\CMS\Fluid\View\StandaloneView $standAloneView */
		$standAloneView = $this->objectManager->get('TYPO3\\CMS\\Fluid\\View\\StandaloneView');

		$extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK);
		$templateRootPath = GeneralUtility::getFileAbsFileName($extbaseFrameworkConfiguration['view']['templateRootPaths'][count($extbaseFrameworkConfiguration['view']['templateRootPaths']) - 1]);
		$templatePathAndFilename = $templateRootPath . 'DocumentTemplates/Table.html';

		$standAloneView->setFormat('html');
		$standAloneView->setTemplatePathAndFilename($templatePathAndFilename);
		$standAloneView->setTemplateRootPaths($extbaseFrameworkConfiguration['view']['templateRootPaths']);
		$standAloneView->setLayoutRootPaths($extbaseFrameworkConfiguration['view']['layoutRootPaths']);
		$standAloneView->setPartialRootPaths($extbaseFrameworkConfiguration['view']['partialRootPaths']);

		return $standAloneView;
	}

	/**
	 * @param \Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile
	 *
	 * @return string
	 */
	public function generatePDF(\Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile)
	{
//		$outputFilePath = \Artif\ArtifEqhm\Utility\NameGenerator::generateFileName('pdf', $this->practice->getUid());
		$outputFilePath = \Artif\ArtifEqhm\Utility\NameGenerator::getFileNamePath('pdf', $documentFile);
		$view = $this->getView();

		$view->assign('pathSite', PATH_site);
		$view->assign('practice', $this->practice);
		$view->assign('documentFile', $documentFile);

		$pdfOptions = [
			'no-outline',
			'margin-top'    => 15,
			'margin-right'  => 5,
			'margin-bottom' => 35,
			'margin-left'   => 5,
			'binary' => '/usr/local/bin/wkhtmltopdf',

			'header-html'     => $this->headerAndFooterGenerator->generateHeader($this->practice),
			'footer-html'     => $this->headerAndFooterGenerator->generateFooter($documentFile),

			'user-style-sheet' => GeneralUtility::getFileAbsFileName('EXT:artif_eqhm/'.$this->settings['pdf.']['cssForTable']),
		];

//        DebuggerUtility::var_dump($pdfOptions);die;
		$pdf = new Pdf(
            $this->objectManager->get(
                \Artif\ArtifEqhm\Utility\CssToInline::class,
                $view->render(),
                file_get_contents($pdfOptions['user-style-sheet'])
            )->emogrify()
        );

		$pdf->setOptions($pdfOptions);
		if (!$pdf->saveAs($outputFilePath)) {
			return $pdf->getError();
		}

		return $outputFilePath;
	}


}