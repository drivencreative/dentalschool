<?php

namespace Artif\ArtifEqhm\Domain\Repository;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

use Artif\ArtifEqhm\Domain\Model\Document;
use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\Practice;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\QueryBuilder;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Documents
 */
class DocumentRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject() {
        /** @var $defaultQuerySettings Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        // add the pid constraint
        $defaultQuerySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($defaultQuerySettings);
    }

    /**
     * @param int $version
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findBaseVersions($version = 0)
    {
        $query = $this->createQuery();
        $query->matching($query->equals('basedOn', $version));
        $query->setOrderings(
            array (
                'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
            )
        );
        return $query->execute();
    }

    /**
     * @param Document $document
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllClonesOf(Document $document)
    {
        $query = $this->createQuery();
        $query->matching($query->equals('cloneOf', $document));
        $query->setOrderings(
            array (
                'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING
            )
        );
        $query->statement();
        return $query->execute();
    }

    /**
     * @param Document $document
     * @return \Doctrine\DBAL\Driver\Statement|int
     */
    public function updateAllClonesOf(Document $document)
    {
        /** @var QueryBuilder $queryBuilder */
        $queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable('tx_artifeqhm_domain_model_document');
        $queryBuilder
            ->update('tx_artifeqhm_domain_model_document')
            ->where(
                $queryBuilder->expr()->eq('clone_of', $document->getUid())
            )
            ->set('notify', 1);
        return $queryBuilder->execute();
    }

    /**
     * @param Practice $practice
     * @param Document $baseVersion
     *
     * @return Document|object
     */
    public function findByPracticeAndBaseVersion(Practice $practice, Document $baseVersion)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->equals('practice', $practice),
                $query->equals('basedOn', $baseVersion)

            )
        );
        return $query->execute()->getFirst();
    }

    /**
     * @param Practice $practice
     *
     * @return Document|object
     */
    public function findByPractice(Practice $practice)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->equals('practice', $practice)
//            $query->logicalOr(
//                $query->equals('practice', $practice),
//                $query->equals('practice', 0)
//
//            )
        );
        return $query->execute();
    }

    /**
     * @param mixed $categories
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCategoryAndMaster($categories)
    {
        $categories = is_array($categories) ? $categories : explode(',', $categories);
        if (!$categories) return [];
        $query = $this->createQuery();
        $query->setQuerySettings($query->getQuerySettings()->setRespectStoragePage(false));
        $constraint = [];
        foreach ($categories as $category) {
            $constraint[] = $query->contains('categories', $category);
        }
        $query->matching(
            $query->logicalAnd(
                $query->equals('practice.master', true),
                $query->logicalOr($constraint)
            )
        );

        return $query->execute();
    }

    /**
     * @param Practice $practice
     * @param mixed $categories
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCategoryAndIsDevice(Practice $practice, $categories)
    {
        $categories = is_array($categories) ? $categories : explode(',', $categories);
        if (!$categories) return [];
        $query = $this->createQuery();
        $query->setQuerySettings($query->getQuerySettings()->setRespectStoragePage(false));
        $constraint = [];
        foreach ($categories as $category) {
            $constraint[] = $query->contains('categories', $category);
        }
        $query->matching(
            $query->logicalAnd(
                $query->equals('practice', $practice),
                $query->equals('isDevice', true),
                $query->logicalOr($constraint)
            )
        );

        return $query->execute();
    }

    /**
     * @param mixed $categories
     * @param $practice
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCategoryAndPractice($categories, $practice=null)
    {
        $categories = is_array($categories) ? $categories : explode(',', $categories);
        if (!$categories) return [];
        $query = $this->createQuery();
        $and = [];
        foreach ($categories as $category) {
            if ($category) {
                $and[] = $query->contains('categories', $category);
            }
        }
        if ($practice) {
            $and[] = $query->equals('practice', $practice);
//            $and[] = $query->logicalOr(
//                $query->equals('practice', $practice),
//                $query->equals('practice', 0)
//            );
        }
        $constraint = $query->logicalAnd($and);
        $query->matching($constraint);
        return $query->execute();
    }

    /**
     * @param $practice
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllByPractice(Practice $practice)
    {
        $query = $this->createQuery();
//        $or = [];
//        if ($practice) {
//            $or[] = $query->equals('practice', $practice);
//            // Return records which are available for all practices
//            $or[] = $query->equals('practice', 0);
//
//        }
//        $constraint = $query->logicalOr($or);
        $constraint = $query->equals('practice', $practice);
        $query->matching($constraint);
        return $query->execute();
    }

    /**
     * @param Practice $practice
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findAllNotify(Practice $practice=null)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->equals('practice', $practice),
                $query->equals('notify', true)

            )
        );
        return $query->execute();
    }

    /**
     * @param Practice $practice
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findPending(Practice $practice=null)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->logicalAnd(
                $query->equals('practice', $practice),
                $query->equals('generic', false),
                $query->logicalNot(
                    $query->in('typ', ['NoneEditablePortableDocumentFormat', 'Link'])
                )
            )
        );

        return $query->execute();
    }


    /**
     * Persist all Objects
     */
    public function persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}
