<?php

namespace Artif\ArtifEqhm\Domain\Repository;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/
use \TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * The repository for Practices
 */
class PracticeRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject()
    {
        /** @var $defaultQuerySettings Typo3QuerySettings */
        $this->defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        // don't add the pid constraint
        $this->defaultQuerySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($this->defaultQuerySettings);
    }

    public function findByCurrentUser($user)
    {
        $query = $this->createQuery();
        $query->matching(
            $query->contains('frontendUser', $user)
        );

        return $query->execute()->getFirst();
    }

    /**
     * Persist all Objects
     */
    public function persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}
