<?php

namespace Artif\ArtifEqhm\Domain\Repository;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

use TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings;

/**
 * The repository for Device
 */
class DeviceRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * @var array
     */
    protected $defaultOrderings = [
        'name' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject() {
        /** @var $defaultQuerySettings Typo3QuerySettings */
        $defaultQuerySettings = $this->objectManager->get(Typo3QuerySettings::class);
        $defaultQuerySettings->setRespectStoragePage(false);
        $this->setDefaultQuerySettings($defaultQuerySettings);
    }

    /**
     * @param mixed $categories
     * @param \Artif\ArtifEqhm\Domain\Model\Practice $practice
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByPracticeAndCategory($practice, $categories)
    {
        $categories = is_array($categories) ? $categories : explode(',', $categories);
        if (!$categories) return [];

        $query = $this->createQuery();
        $constraint = [];
        foreach ($categories as $category) {
            if ($category) {
                $constraint[] = $query->contains('categories', $category);
                $constraint[] = $query->equals('practice', $practice);
            }
        }
        $query->matching($query->logicalAnd($constraint));

        return $query->execute();
    }

    /**
     * @param mixed $categories
     *
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findByCategoryAndMaster($categories)
    {
        $categories = is_array($categories) ? $categories : explode(',', $categories);
        if (!$categories) return [];
        $query = $this->createQuery();
        $query->setQuerySettings($query->getQuerySettings()->setRespectStoragePage(false));
        $constraint = [];
        foreach ($categories as $category) {
            $constraint[] = $query->contains('categories', $category);
        }
        $query->matching(
            $query->logicalAnd(
                $query->equals('practice.master', true),
                $query->logicalOr($constraint)
            )
        );

        return $query->execute();
    }

    /**
     * Persist all Objects
     */
    public function persistAll()
    {
        $this->persistenceManager->persistAll();
    }
}
