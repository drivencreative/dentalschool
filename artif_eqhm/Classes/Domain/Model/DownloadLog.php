<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/
class DownloadLog extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * crdate
     *
     * @var \DateTime
     */
    protected $crdate = null;

    /**
     * documentFile
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile>
     * @cascade remove
     */
    protected $documentFile = null;

    /**
     * frontendUser
     *
     * @var \Artif\ArtifEqhm\Domain\Model\FrontendUser
     */
    protected $frontendUser = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->frontendUser = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return \DateTime
     */
    public function getCrdate()
    {
        return $this->crdate;
    }

    /**
     * @param \DateTime $crdate
     */
    public function setCrdate($crdate)
    {
        $this->crdate = $crdate;
    }

    /**
     * Returns the frontendUser
     *
     * @return \Artif\ArtifEqhm\Domain\Model\FrontendUser $frontendUser
     */
    public function getFrontendUser()
    {
        return $this->frontendUser;
    }

    /**
     * Sets the frontendUser
     *
     * @param \Artif\ArtifEqhm\Domain\Model\FrontendUser $frontendUser
     *
     * @return void
     */
    public function setFrontendUser(\Artif\ArtifEqhm\Domain\Model\FrontendUser $frontendUser)
    {
        $this->frontendUser = $frontendUser;
    }

    /**
     * Returns the documentFile
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile> $documentFile
     */
    public function getDocumentFile()
    {
        return $this->documentFile;
    }

    /**
     * Sets the documentFile
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile> $documentFile
     *
     * @return void
     */
    public function setDocumentFile(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $documentFile)
    {
        $this->documentFile = $documentFile;
    }

}