<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * DocumentFile
 */
class DocumentFile extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * content
     *
     * @var string
     */
    protected $content = '';

    /**
     * additionalContent
     *
     * @var string
     */
    protected $additionalContent = '';

    /**
     * file
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $file = null;

    /**
     * version
     *
     * @var int
     * @validate NotEmpty
     */
    protected $version = 0;

    /**
     * signed
     *
     * @var \DateTime
     */
    protected $signed = null;

    /**
     * signatureImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $signatureImage = null;

    /**
     * signatureName
     *
     * @var string
     */
    protected $signatureName = '';

    /**
     * document
     *
     * @var \Artif\ArtifEqhm\Domain\Model\Document
     */
    protected $document = null;

    /**
     * Signature index
     *
     * @var int
     */
    protected $signatureIndex = 9999;

    /**
     * downloadLog
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DownloadLog>
     * @cascade remove
     */
    protected $downloadLog = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->downloadLog = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the content
     *
     * @return string $content
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Sets the content
     *
     * @param string $content
     *
     * @return void
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * Returns the additionalContent
     *
     * @return string $additionalContent
     */
    public function getAdditionalContent()
    {
        return $this->additionalContent;
    }

    /**
     * Sets the additionalContent
     *
     * @param string $additionalContent
     *
     * @return void
     */
    public function setAdditionalContent($additionalContent)
    {
        $this->additionalContent = $additionalContent;
    }

    /**
     * Returns the file
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Sets the file
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $file
     *
     * @return void
     */
    public function setFile(\TYPO3\CMS\Extbase\Domain\Model\FileReference $file)
    {
        $this->file = $file;
    }

    /**
     * Returns the version
     *
     * @return int $version
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Sets the version
     *
     * @param int $version
     *
     * @return void
     */
    public function setVersion($version)
    {
        $this->version = $version;
    }

    /**
     * Returns the signed
     *
     * @return \DateTime $signed
     */
    public function getSigned()
    {
        return $this->signed;
    }

    /**
     * Sets the signed
     *
     * @param \DateTime $signed
     *
     * @return void
     */
    public function setSigned(\DateTime $signed = null)
    {
        $this->signed = $signed;
    }

    /**
     * Returns the signatureImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $signatureImage
     */
    public function getSignatureImage()
    {
        return $this->signatureImage;
    }

    /**
     * Sets the signatureImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $signatureImage
     *
     * @return void
     */
    public function setSignatureImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $signatureImage = null)
    {
        $this->signatureImage = $signatureImage;
    }

    /**
     * Returns the signatureName
     *
     * @return string $signatureName
     */
    public function getSignatureName()
    {
        return $this->signatureName;
    }

    /**
     * Sets the signatureName
     *
     * @param string $signatureName
     *
     * @return void
     */
    public function setSignatureName($signatureName = '')
    {
        $this->signatureName = $signatureName;
    }

    /**
     * @return string
     */
    public function getPending()
    {
        return $this->pending;
    }

    /**
     * @param string $pending
     */
    public function setPending($pending)
    {
        $this->pending = $pending;
    }

    /**
     * @return \Artif\ArtifEqhm\Domain\Model\Document
     */
    public function getDocument()
    {
        return $this->document;
    }

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     */
    public function setDocument(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        $this->document = $document;
    }

    /**
     * resetUid
     */
    public function resetUid()
    {
        $this->uid = null;
        $this->_setClone(false);
    }

    /**
     * @return bool
     */
    public function isSigned()
    {
        return $this->signed ? true : false;
    }


    /**
     * Set Signature Image as File Reference
     *
     * @param DocumentFile $signatureImage
     * @return null
     */
    public function setSignatureImageFileReference(DocumentFile $signatureImage)
    {
//        DebuggerUtility::var_dump($signatureImage->getSignatureImage());die;

        if ($signatureImage->getSignatureImage() != null) {
            if (!$this->getSignatureImage()) {
                $this->setSignatureImage(GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class));
            }
            $this->getSignatureImage()->setOriginalResource($signatureImage->getSignatureImage()->getOriginalResource());
        }
    }

    /**
     * Check if file is image
     *
     * @return bool
     */
    public function isImage()
    {
        $allowedExt = explode(',', $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext']);
        if (($key = array_search('pdf', $allowedExt)) !== false) {
            unset($allowedExt[$key]);
        }
        if ($this->getFile()) {
            if ($this->getFile()->getOriginalResource()) {
                return ($this->getFile() && in_array($this->getFile()->getOriginalResource()->getExtension(), $allowedExt)) ? 1 : 0;
            }
            return 0;
        }
        return 0;
    }

    /**
     * @return int
     */
    public function getSignatureIndex()
    {
        return $this->signatureIndex;
    }

    /**
     * @param string $signatureIndex
     */
    public function setSignatureIndex($signatureIndex)
    {
        $this->signatureIndex = $signatureIndex;
    }

    /**
     * Adds a DownloadLog
     *
     * @param \Artif\ArtifEqhm\Domain\Model\DownloadLog $downloadLog
     *
     * @return void
     */
    public function addDownloadLog(\Artif\ArtifEqhm\Domain\Model\DownloadLog $downloadLog)
    {
        $this->downloadLog->attach($downloadLog);
    }

    /**
     * Removes a DownloadLog
     *
     * @param \Artif\ArtifEqhm\Domain\Model\DownloadLog $downloadLogToRemove The DownloadLog to be removed
     *
     * @return void
     */
    public function removeDownloadLog(\Artif\ArtifEqhm\Domain\Model\DownloadLog $downloadLogToRemove)
    {
        $this->downloadLog->detach($downloadLogToRemove);
    }

    /**
     * Returns the downloadLog
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DownloadLog> $downloadLog
     */
    public function getDownloadLog()
    {
        return $this->downloadLog;
    }

    /**
     * Sets the downloadLog
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DownloadLog> $downloadLog
     *
     * @return void
     */
    public function setDownloadLog(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $downloadLog)
    {
        $this->downloadLog = $downloadLog;
    }

    /**
     * Check if Document file is new
     *
     * @return bool
     */
    public function isNew()
    {
        return !$this->getDownloadLog()->count() && $this->isSigned();
    }

}
