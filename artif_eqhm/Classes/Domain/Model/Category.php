<?php
namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * Signature
 */
class Category extends \TYPO3\CMS\Extbase\Domain\Model\Category
{

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category>
     */
    protected $categories = null;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Document>
     */
    protected $items = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $this->items = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category>
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category> $categories
     */
    public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Document>
     */
    public function getItems(): \TYPO3\CMS\Extbase\Persistence\ObjectStorage
    {
        return $this->items;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Document> $items
     */
    public function setItems(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $items)
    {
        $this->items = $items;
    }

}
