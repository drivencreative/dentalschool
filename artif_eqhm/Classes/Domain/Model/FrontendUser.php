<?php
namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * FrontendUser
 */
class FrontendUser extends \TYPO3\CMS\Extbase\Domain\Model\FrontendUser
{
    public function isReadOnly($readOnlyGroup){
        foreach ($this->getUsergroup()->toArray() as $userGroup){

            return ($userGroup->getUid() == $readOnlyGroup) ? true : false;
        }
    }
}
