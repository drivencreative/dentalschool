<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * Terms
 */
class Terms extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * document
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @cascade remove
	 */
	protected $document = null;

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 *
	 * @return void
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Returns the document
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
	 */
	public function getDocument()
	{
		return $this->document;
	}

	/**
	 * Sets the document
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $document
	 *
	 * @return void
	 */
	public function setDocument(\TYPO3\CMS\Extbase\Domain\Model\FileReference $document)
	{
		$this->document = $document;
	}
}
