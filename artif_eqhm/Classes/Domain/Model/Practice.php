<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Practice
 */
class Practice extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/**
	 * name
	 *
	 * @var string
     * @validate NotEmpty
	 */
	protected $name = '';

	/**
	 * street
	 *
	 * @var string
     * @validate NotEmpty
	 */
	protected $street = '';

	/**
	 * number
	 *
	 * @var string
     * @validate NotEmpty
	 */
	protected $number = '';

	/**
	 * zip
	 *
	 * @var string
     * @validate NotEmpty
	 */
	protected $zip = '';

	/**
	 * city
	 *
	 * @var string
     * @validate NotEmpty
	 */
	protected $city = '';

	/**
	 * country
	 *
	 * @var string
	 */
	protected $country = '';

	/**
	 * master
	 *
	 * @var bool
	 */
	protected $master = 0;

	/**
	 * terms
	 *
	 * @var bool
     * @validate Boolean(is=true)
	 */
	protected $terms = 0;

	/**
     * dentalAssistant
     *
     * @var string
	 **/
    protected $dentalAssistant = '';

    /**
     * medicalOfficers
     *
     * @var string
     * @validate NotEmpty
     **/
    protected $medicalOfficers = '';

    /**
     * medicalDevice
     *
     * @var string
     * @validate NotEmpty
     **/
    protected $medicalDevice = '';

    /**
     * securityOfficers
     *
     * @var string
     * @validate NotEmpty
     **/
    protected $securityOfficers = '';

    /**
     * laserProtection
     *
     * @var string
     * @validate NotEmpty
     **/
    protected $laserProtection = '';

    /**
     * leadingZfa
     *
     * @var string
     * @validate NotEmpty
     **/
    protected $leadingZfa = '';

    /**
     * leadingZfaRepresentative
     *
     * @var string
     **/
    protected $leadingZfaRepresentative = '';

    /**
     * zfas
     *
     * @var string
     **/
    protected $zfas = '';

    /**
     * zfasRepresentative
     *
     * @var string
     **/
    protected $zfasRepresentative = '';

    /**
     * reception
     *
     * @var string
     **/
    protected $reception = '';

    /**
     * receptionRepresentation
     *
     * @var string
     **/
    protected $receptionRepresentation = '';

    /**
     * accounting
     *
     * @var string
     **/
    protected $accounting = '';

    /**
     * billingRepresentative
     *
     * @var string
     **/
    protected $billingRepresentative = '';

    /**
     * practiceLaboratory
     *
     * @var string
     **/
    protected $practiceLaboratory = '';

    /**
     * practiceLaboratoryRepresentative
     *
     * @var string
     **/
    protected $practiceLaboratoryRepresentative = '';

    /**
     * staff
     *
     * @var string
     **/
    protected $staff = '';

    /**
     * personalRepresentation
     *
     * @var string
     **/
    protected $personalRepresentation = '';

    /**
     * financialAccounting
     *
     * @var string
     **/
    protected $financialAccounting = '';

    /**
     * financialAccountingRepresentative
     *
     * @var string
     **/
    protected $financialAccountingRepresentative = '';

    /**
     * purchase
     *
     * @var string
     **/
    protected $purchase = '';

    /**
     * shoppingRepresentation
     *
     * @var string
     **/
    protected $shoppingRepresentation = '';

    /**
     * itOrganization
     *
     * @var string
     **/
    protected $itOrganization = '';

    /**
     * itOrganizationRepresentative
     *
     * @var string
     **/
    protected $itOrganizationRepresentative = '';


	/**
	 * logo
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @cascade remove
	 */
	protected $logo = null;

	/**
	 * signatures
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Signature>
	 * @cascade remove
	 */
	protected $signatures = null;

	/**
	 * frontendUser
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\FrontendUser>
	 */
	protected $frontendUser = null;

    /**
     * @var int
     */
    protected $tstamp;

    /**
     * hygieneplan
     *
     * @var \Artif\ArtifEqhm\Domain\Model\Document
     */
    protected $hygieneplan = null;

    /**
     * organigramm
     *
     * @var \Artif\ArtifEqhm\Domain\Model\Document
     */
    protected $organigramm;

    /**
     * qmHandbuch
     *
     * @var \Artif\ArtifEqhm\Domain\Model\Document
     */
    protected $qmHandbuch;

    /**
     * initialized
     *
     * @var bool
     */
    protected $initialized;


    /**
	 * __construct
	 */
	public function __construct()
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects()
	{
		$this->signatures = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->frontendUser = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Error\Result
	 */
	public function validation()
	{
        /* @var $validator \TYPO3\CMS\Extbase\Validation\Validator\ConjunctionValidator */
        $validator = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Validation\ValidatorResolver::class)->getBaseValidatorConjunction(get_class($this));
        return $validator->validate($this);
	}

	/**
	 * @return bool
	 */
	public function isEverythingFilled()
	{
        return !$this->validation()->hasErrors();
	}

	/**
	 * @return string
	 */
	public function getHeader()
	{

		$header = $this->getName() . "\r\n" . $this->getStreet() . ' ' . $this->getNumber() . ' ' . $this->getZip() . ' ' . $this->getCity() . ' ' . $this->getCountry();
		return $header;
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 *
	 * @return void
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Returns the street
	 *
	 * @return string $street
	 */
	public function getStreet()
	{
		return $this->street;
	}

	/**
	 * Sets the street
	 *
	 * @param string $street
	 *
	 * @return void
	 */
	public function setStreet($street)
	{
		$this->street = $street;
	}

	/**
	 * Returns the number
	 *
	 * @return string $number
	 */
	public function getNumber()
	{
		return $this->number;
	}

	/**
	 * Sets the number
	 *
	 * @param string $number
	 *
	 * @return void
	 */
	public function setNumber($number)
	{
		$this->number = $number;
	}

	/**
	 * Returns the zip
	 *
	 * @return string $zip
	 */
	public function getZip()
	{
		return $this->zip;
	}

	/**
	 * Sets the zip
	 *
	 * @param string $zip
	 *
	 * @return void
	 */
	public function setZip($zip)
	{
		$this->zip = $zip;
	}

	/**
	 * Returns the city
	 *
	 * @return string $city
	 */
	public function getCity()
	{
		return $this->city;
	}

	/**
	 * Sets the city
	 *
	 * @param string $city
	 *
	 * @return void
	 */
	public function setCity($city)
	{
		$this->city = $city;
	}

	/**
	 * Returns the country
	 *
	 * @return string $country
	 */
	public function getCountry()
	{
		return $this->country;
	}

	/**
	 * Sets the country
	 *
	 * @param string $country
	 *
	 * @return void
	 */
	public function setCountry($country)
	{
		$this->country = $country;
	}

	/**
	 * Returns the terms
	 *
	 * @return bool $terms
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	/**
	 * Sets the terms
	 *
	 * @param bool $terms
	 *
	 * @return void
	 */
	public function setTerms($terms)
	{
		$this->terms = $terms;
	}

	/**
	 * Returns the logo
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
	 */
	public function getLogo()
	{
		return $this->logo;
	}

	/**
	 * Sets the logo
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $logo
	 *
	 * @return void
	 */
	public function setLogo(\TYPO3\CMS\Extbase\Domain\Model\FileReference $logo)
	{
		$this->logo = $logo;
	}

	/**
	 * Adds a Signature
	 *
	 * @param \Artif\ArtifEqhm\Domain\Model\Signature $signature
	 *
	 * @return void
	 */
	public function addSignature(\Artif\ArtifEqhm\Domain\Model\Signature $signature)
	{
		$this->signatures->attach($signature);
	}

	/**
	 * Removes a Signature
	 *
	 * @param \Artif\ArtifEqhm\Domain\Model\Signature $signatureToRemove The Signature to be removed
	 *
	 * @return void
	 */
	public function removeSignature(\Artif\ArtifEqhm\Domain\Model\Signature $signatureToRemove)
	{
		$this->signatures->detach($signatureToRemove);
	}

	/**
	 * Returns the signatures
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Signature> $signatures
	 */
	public function getSignatures()
	{
		return $this->signatures;
	}

	/**
	 * Sets the signatures
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Signature> $signatures
	 *
	 * @return void
	 */
	public function setSignatures(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $signatures)
	{
		$this->signatures = $signatures;
	}

	/**
	 * Returns the frontendUser
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\FrontendUser> $frontendUser
	 */
	public function getFrontendUser()
	{
		return $this->frontendUser;
	}

	/**
	 * Sets the frontendUser
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\FrontendUser> $frontendUser
	 *
	 * @return void
	 */
	public function setFrontendUser(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $frontendUser)
	{
		$this->frontendUser = $frontendUser;
	}

    /**
     * @return string
     */
    public function getDentalAssistant ()
    {
        return $this->dentalAssistant;
    }

    /**
     * @param string $dentalAssistant
     */
    public function setDentalAssistant ($dentalAssistant)
    {
        $this->dentalAssistant = $dentalAssistant;
    }

    /**
     * @return string
     */
    public function getMedicalOfficers ()
    {
        return $this->medicalOfficers;
    }

    /**
     * @param string $medicalOfficers
     */
    public function setMedicalOfficers ($medicalOfficers)
    {
        $this->medicalOfficers = $medicalOfficers;
    }

    /**
     * @return string
     */
    public function getMedicalDevice ()
    {
        return $this->medicalDevice;
    }

    /**
     * @param string $medicalDevice
     */
    public function setMedicalDevice ($medicalDevice)
    {
        $this->medicalDevice = $medicalDevice;
    }

    /**
     * @return string
     */
    public function getSecurityOfficers ()
    {
        return $this->securityOfficers;
    }

    /**
     * @param string $securityOfficers
     */
    public function setSecurityOfficers ($securityOfficers)
    {
        $this->securityOfficers = $securityOfficers;
    }

    /**
     * @return string
     */
    public function getLaserProtection ()
    {
        return $this->laserProtection;
    }

    /**
     * @param string $laserProtection
     */
    public function setLaserProtection ($laserProtection)
    {
        $this->laserProtection = $laserProtection;
    }

    /**
     * @return string
     */
    public function getLeadingZfa ()
    {
        return $this->leadingZfa;
    }

    /**
     * @param string $leadingZfa
     */
    public function setLeadingZfa ($leadingZfa)
    {
        $this->leadingZfa = $leadingZfa;
    }

    /**
     * @return string
     */
    public function getLeadingZfaRepresentative ()
    {
        return $this->leadingZfaRepresentative;
    }

    /**
     * @param string $leadingZfaRepresentative
     */
    public function setLeadingZfaRepresentative ($leadingZfaRepresentative)
    {
        $this->leadingZfaRepresentative = $leadingZfaRepresentative;
    }

    /**
     * @return string
     */
    public function getZfas ()
    {
        return $this->zfas;
    }

    /**
     * @param string $zfas
     */
    public function setZfas ($zfas)
    {
        $this->zfas = $zfas;
    }

    /**
     * @return string
     */
    public function getZfasRepresentative ()
    {
        return $this->zfasRepresentative;
    }

    /**
     * @param string $zfasRepresentative
     */
    public function setZfasRepresentative ($zfasRepresentative)
    {
        $this->zfasRepresentative = $zfasRepresentative;
    }

    /**
     * @return string
     */
    public function getReception ()
    {
        return $this->reception;
    }

    /**
     * @param string $reception
     */
    public function setReception ($reception)
    {
        $this->reception = $reception;
    }

    /**
     * @return string
     */
    public function getReceptionRepresentation ()
    {
        return $this->receptionRepresentation;
    }

    /**
     * @param string $receptionRepresentation
     */
    public function setReceptionRepresentation ($receptionRepresentation)
    {
        $this->receptionRepresentation = $receptionRepresentation;
    }

    /**
     * @return string
     */
    public function getAccounting ()
    {
        return $this->accounting;
    }

    /**
     * @param string $accounting
     */
    public function setAccounting ($accounting)
    {
        $this->accounting = $accounting;
    }

    /**
     * @return string
     */
    public function getBillingRepresentative ()
    {
        return $this->billingRepresentative;
    }

    /**
     * @param string $billingRepresentative
     */
    public function setBillingRepresentative ($billingRepresentative)
    {
        $this->billingRepresentative = $billingRepresentative;
    }

    /**
     * @return string
     */
    public function getPracticeLaboratory ()
    {
        return $this->practiceLaboratory;
    }

    /**
     * @param string $practiceLaboratory
     */
    public function setPracticeLaboratory ($practiceLaboratory)
    {
        $this->practiceLaboratory = $practiceLaboratory;
    }

    /**
     * @return string
     */
    public function getPracticeLaboratoryRepresentative ()
    {
        return $this->practiceLaboratoryRepresentative;
    }

    /**
     * @param string $practiceLaboratoryRepresentative
     */
    public function setPracticeLaboratoryRepresentative ($practiceLaboratoryRepresentative)
    {
        $this->practiceLaboratoryRepresentative = $practiceLaboratoryRepresentative;
    }

    /**
     * @return string
     */
    public function getStaff ()
    {
        return $this->staff;
    }

    /**
     * @param string $staff
     */
    public function setStaff ($staff)
    {
        $this->staff = $staff;
    }

    /**
     * @return string
     */
    public function getPersonalRepresentation ()
    {
        return $this->personalRepresentation;
    }

    /**
     * @param string $personalRepresentation
     */
    public function setPersonalRepresentation ($personalRepresentation)
    {
        $this->personalRepresentation = $personalRepresentation;
    }

    /**
     * @return string
     */
    public function getFinancialAccounting ()
    {
        return $this->financialAccounting;
    }

    /**
     * @param string $financialAccounting
     */
    public function setFinancialAccounting ($financialAccounting)
    {
        $this->financialAccounting = $financialAccounting;
    }

    /**
     * @return string
     */
    public function getFinancialAccountingRepresentative ()
    {
        return $this->financialAccountingRepresentative;
    }

    /**
     * @param string $financialAccountingRepresentative
     */
    public function setFinancialAccountingRepresentative ($financialAccountingRepresentative)
    {
        $this->financialAccountingRepresentative = $financialAccountingRepresentative;
    }

    /**
     * @return string
     */
    public function getPurchase ()
    {
        return $this->purchase;
    }

    /**
     * @param string $purchase
     */
    public function setPurchase ($purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * @return string
     */
    public function getShoppingRepresentation ()
    {
        return $this->shoppingRepresentation;
    }

    /**
     * @param string $shoppingRepresentation
     */
    public function setShoppingRepresentation ($shoppingRepresentation)
    {
        $this->shoppingRepresentation = $shoppingRepresentation;
    }

    /**
     * @return string
     */
    public function getItOrganization ()
    {
        return $this->itOrganization;
    }

    /**
     * @param string $itOrganization
     */
    public function setItOrganization ($itOrganization)
    {
        $this->itOrganization = $itOrganization;
    }

    /**
     * @return string
     */
    public function getItOrganizationRepresentative ()
    {
        return $this->itOrganizationRepresentative;
    }

    /**
     * @param string $itOrganizationRepresentative
     */
    public function setItOrganizationRepresentative ($itOrganizationRepresentative)
    {
        $this->itOrganizationRepresentative = $itOrganizationRepresentative;
    }

    /**
     * @return int $tstamp
     */
    public function getTstamp() {
        return $this->tstamp;
    }

    /**
     * @return \Artif\ArtifEqhm\Domain\Model\Document
     */
    public function getHygieneplan()
    {
        return $this->hygieneplan;
    }

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\Document $hygieneplan
     */
    public function setHygieneplan (\Artif\ArtifEqhm\Domain\Model\Document $hygieneplan)
    {
        $this->hygieneplan = $hygieneplan;
    }

    /**
     * @return bool
     */
    public function isMaster ()
    {
        return $this->master;
    }

    /**
     * @return bool
     */
    public function getMaster ()
    {
        return $this->master;
    }

    /**
     * @param bool $master
     */
    public function setMaster ($master)
    {
        $this->master = $master;
    }

    /**
     * @return \Artif\ArtifEqhm\Domain\Model\Document
     */
    public function getOrganigramm()
    {
        return $this->organigramm;
    }

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\Document $organigramm
     */
    public function setOrganigramm (\Artif\ArtifEqhm\Domain\Model\Document $organigramm)
    {
        $this->organigramm = $organigramm;
    }
    /**
     * @return \Artif\ArtifEqhm\Domain\Model\Document
     */
    public function getQmHandbuch()
    {
        return $this->qmHandbuch;
    }

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\Document $qmHandbuch
     */
    public function setQmHandbuch(\Artif\ArtifEqhm\Domain\Model\Document $qmHandbuch)
    {
        $this->qmHandbuch = $qmHandbuch;
    }

    /**
     * @return bool
     */
    public function getInitialized()
    {
        return $this->initialized;
    }

    /**
     * @param bool $initialized
     */
    public function setInitialized($initialized)
    {
        $this->initialized = $initialized;
    }
}
