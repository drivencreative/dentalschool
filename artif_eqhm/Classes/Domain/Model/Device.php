<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * Device
 */
class Device extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/**
	 * type
	 *
	 * @var string
	 */
	protected $type = '';

	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * manufacturer
	 *
	 * @var string
	 */
	protected $manufacturer = '';

	/**
	 * ceMark
	 *
	 * @var string
	 */
	protected $ceMark = '';

	/**
	 * serialNumber
	 *
	 * @var string
	 */
	protected $serialNumber = '';

	/**
	 * yearOfConstruction
	 *
	 * @var string
	 */
	protected $yearOfConstruction = '';

	/**
	 * space
	 *
	 * @var string
	 */
	protected $space = '';

	/**
	 * number
	 *
	 * @var int
	 */
	protected $number = 0;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category>
     */
    protected $categories = null;

    /**
     * space
     *
     * @var string
     */
    protected $notes = '';

    /**
     * practice
     *
     * @var \Artif\ArtifEqhm\Domain\Model\Practice
     */
    protected $practice = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * @return string
     */
    public function getType ()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType ($type)
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getName ()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName ($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getManufacturer ()
    {
        return $this->manufacturer;
    }

    /**
     * @param string $manufacturer
     */
    public function setManufacturer ($manufacturer)
    {
        $this->manufacturer = $manufacturer;
    }

    /**
     * @return string
     */
    public function getCeMark ()
    {
        return $this->ceMark;
    }

    /**
     * @param string $ceMark
     */
    public function setCeMark ($ceMark)
    {
        $this->ceMark = $ceMark;
    }

    /**
     * @return string
     */
    public function getSerialNumber ()
    {
        return $this->serialNumber;
    }

    /**
     * @param string $serialNumber
     */
    public function setSerialNumber ($serialNumber)
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @return string
     */
    public function getYearOfConstruction ()
    {
        return $this->yearOfConstruction;
    }

    /**
     * @param string $yearOfConstruction
     */
    public function setYearOfConstruction ($yearOfConstruction)
    {
        $this->yearOfConstruction = $yearOfConstruction;
    }

    /**
     * @return string
     */
    public function getSpace ()
    {
        return $this->space;
    }

    /**
     * @param string $space
     */
    public function setSpace ($space)
    {
        $this->space = $space;
    }

    /**
     * @return string
     */
    public function getNotes ()
    {
        return $this->notes;
    }

    /**
     * @param string $notes
     */
    public function setNotes ($notes)
    {
        $this->notes = $notes;
    }

    /**
     * @return int
     */
    public function getNumber ()
    {
        return $this->number;
    }

    /**
     * @param int $number
     */
    public function setNumber ($number = 0)
    {
        $this->number = $number;
    }

    /**
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category>
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category> $categories
     */
    public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories)
    {
        $this->categories = $categories;
    }

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\Category $category
     *
     * @return void
     */
    public function addCategory(\Artif\ArtifEqhm\Domain\Model\Category $category)
    {
        $this->categories->attach($category);
    }

    /**
     * Returns the practice
     *
     * @return \Artif\ArtifEqhm\Domain\Model\Practice $practice
     */
    public function getPractice()
    {
        return $this->practice;
    }

    /**
     * Sets the practice
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Practice $practice
     *
     * @return void
     */
    public function setPractice(\Artif\ArtifEqhm\Domain\Model\Practice $practice)
    {
        $this->practice = $practice;
    }

}
