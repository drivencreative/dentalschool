<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * TermsLog
 */
class TermsLog extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/**
	 * practice
	 *
	 * @var \Artif\ArtifEqhm\Domain\Model\Practice
	 * @cascade remove
	 */
	protected $practice = null;

    /**
     * frontendUser
     *
     * @var \Artif\ArtifEqhm\Domain\Model\FrontendUser
     */
    protected $frontendUser = null;

	/**
	 * terms
	 *
	 * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
	 * @cascade remove
	 */
	protected $terms = null;

	/**
	 * crdate
	 *
	 * @var \DateTime
	 */
	protected $crdate = null;

    /**
     * Returns the practice
     *
     * @return \Artif\ArtifEqhm\Domain\Model\Practice $practice
     */
    public function getPractice()
    {
        return $this->practice;
    }

    /**
     * Sets the practice
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Practice $practice
     *
     * @return void
     */
    public function setPractice(\Artif\ArtifEqhm\Domain\Model\Practice $practice)
    {
        $this->practice = $practice;
    }

    /**
     * Returns the frontendUser
     *
     * @return \Artif\ArtifEqhm\Domain\Model\FrontendUser $frontendUser
     */
    public function getFrontendUser()
    {
        return $this->frontendUser;
    }

    /**
     * Sets the frontendUser
     *
     * @param \Artif\ArtifEqhm\Domain\Model\FrontendUser $frontendUser
     *
     * @return void
     */
    public function setFrontendUser(\Artif\ArtifEqhm\Domain\Model\FrontendUser $frontendUser)
    {
        $this->frontendUser = $frontendUser;
    }

	/**
	 * Returns the terms
	 *
	 * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $terms
	 */
	public function getTerms()
	{
		return $this->terms;
	}

	/**
	 * Sets the terms
	 *
	 * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $terms
	 *
	 * @return void
	 */
	public function setTerms(\TYPO3\CMS\Extbase\Domain\Model\FileReference $terms)
	{
		$this->terms = $terms;
	}

    /**
     * @return \DateTime
     */
    public function getCrdate ()
    {
        return $this->crdate;
    }

    /**
     * @param \DateTime $crdate
     */
    public function setCrdate ($crdate)
    {
        $this->crdate = $crdate;
    }
}
