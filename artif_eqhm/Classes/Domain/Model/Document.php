<?php

namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * Document
 */
class Document extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
	/**
	 * name
	 *
	 * @var string
	 */
	protected $name = '';

	/**
	 * changedName
	 *
	 * @var string
	 */
	protected $changedName = '';

	/**
	 * typ
	 *
	 * @var string
	 */
	protected $typ = '';

	/**
	 * basedOnDocumentVersion
	 *
	 * @var string
	 */
	protected $basedOnDocumentVersion = '';

	/**
	 * documentFiles
	 *
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile>
	 * @cascade remove
	 */
	protected $documentFiles = null;

	/**
	 * practice
	 *
	 * @var \Artif\ArtifEqhm\Domain\Model\Practice
	 */
	protected $practice = null;

	/**
	 * basedOn
	 *
	 * @var \Artif\ArtifEqhm\Domain\Model\Document
	 */
	protected $basedOn = null;

	/**
	 * cloneOf
	 *
	 * @var \Artif\ArtifEqhm\Domain\Model\Document
	 */
	protected $cloneOf = null;

	/**
	 * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category>
	 */
	protected $categories = null;

    /**
     * @var bool
     */
	protected $notify = false;

    /**
     * @var bool
     */
    protected $generic = false;

    /**
     * @var bool
     */
    protected $isHygienePlan = false;

    /**
     * @var bool
     */
    protected $isDevice = false;

    /**
	 * __construct
	 */
	public function __construct()
	{
		//Do not remove the next line: It would break the functionality
		$this->initStorageObjects();
	}

	/**
	 * Initializes all ObjectStorage properties
	 * Do not modify this method!
	 * It will be rewritten on each save in the extension builder
	 * You may modify the constructor of this class instead
	 *
	 * @return void
	 */
	protected function initStorageObjects()
	{
		$this->documentFiles = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
		$this->categories = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
	}

	/**
	 * Reset Uid
	 */
	public function resetUid()
	{
		$this->uid = null;
		$this->_setClone(false);
	}

	public function __clone()
	{
		parent::__clone();
		$this->resetUid();
	}

	/**
	 * @return bool
	 */
	public function isSignable()
	{
        return !in_array($this->getTyp(), ['NoneEditablePortableDocumentFormat', 'Link', 'DeviceList']);
	}

	/**
	 * @return DocumentFile|null
	 */
	public function getNewestVersionOfDocumentFile()
	{
		/** @var \Artif\ArtifEqhm\Domain\Model\DocumentFile $newestVersion */
		$newestVersion = null;
		foreach ($this->documentFiles as $oneDocumentFile) {
			if ($newestVersion == null || $newestVersion->getVersion() < $oneDocumentFile->getVersion()) {
				$newestVersion = $oneDocumentFile;
			}
		}
		return $newestVersion;
	}

    /**
     * @return DocumentFile|null
     */
    public function getNewestVersionOfSignedDocumentFile()
    {
        /** @var \Artif\ArtifEqhm\Domain\Model\DocumentFile $newestVersion */
        $newestVersion = null;
        /** @var \Artif\ArtifEqhm\Domain\Model\DocumentFile $oneDocumentFile */
        foreach ($this->documentFiles as $oneDocumentFile) {
            if (($oneDocumentFile->getSigned() || $this->isGeneric() || !$this->isSignable()) && ($newestVersion == null || $newestVersion->getVersion() < $oneDocumentFile->getVersion())) {
                $newestVersion = $oneDocumentFile;
            }
        }
        return $newestVersion;
    }

    /**
     * @return DocumentFile|null
     */
    public function getBasedOnNewestVersionOfDocumentFile()
    {
        return $this->getBasedOn()->getNewestVersionOfDocumentFile();
    }

//	/**
//	 * Is new Base Version available
//	 * @return bool
//	 */
//	public function getIsNewBaseVersionAvailable()
//	{
//		if($this->getBasedOn()){
//	    if ($this->getBasedOn()->getNewestVersionOfDocumentFile() instanceof \Artif\ArtifEqhm\Domain\Model\DocumentFile)
//			if ($this->getBasedOn()->getNewestVersionOfDocumentFile()->getVersion() > $this->getBasedOnDocumentVersion()) {
//				return true;
//			}
//		}
//		return false;
//	}

    /**
     * Set
     *
     * @param DocumentFile $signatureImage
     */
    public function setSignatureImageFileReference(DocumentFile $signatureImage) {
        if ($signatureImage->getSignatureImage()) {
            if (!$this->getSignatureImage()) {
                $this->setSignatureImage(GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class));
            }
            $this->getSignatureImage()->setOriginalResource($signatureImage->getSignatureImage()->getOriginalResource());
        }
    }

	/**
	 * Returns the changedName
	 *
	 * @return string $changedName
	 */
	public function getChangedName()
	{
		return $this->changedName;
	}

	/**
	 * Sets the changedName
	 *
	 * @param string $changedName
     *
     * @return void
	 */
	public function setChangedName($changedName)
	{
		$this->changedName = $changedName;
	}

	/**
	 * Returns the name
	 *
	 * @return string $name
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * Sets the name
	 *
	 * @param string $name
	 *
	 * @return void
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * Returns the typ
	 *
	 * @return string $typ
	 */
	public function getTyp()
	{
		return $this->typ;
	}

	/**
	 * Sets the typ
	 *
	 * @param string $typ
	 *
	 * @return void
	 */
	public function setTyp($typ)
	{
		$this->typ = $typ;
	}

	/**
	 * Returns the basedOnDocumentVersion
	 *
	 * @return string $basedOnDocumentVersion
	 */
	public function getBasedOnDocumentVersion()
	{
		return $this->basedOnDocumentVersion;
	}

	/**
	 * Sets the basedOnDocumentVersion
	 *
	 * @param string $basedOnDocumentVersion
	 *
	 * @return void
	 */
	public function setBasedOnDocumentVersion($basedOnDocumentVersion)
	{
		$this->basedOnDocumentVersion = $basedOnDocumentVersion;
	}

	/**
	 * Adds a DocumentFile
	 *
	 * @param \Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile
	 *
	 * @return void
	 */
	public function addDocumentFile(\Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile)
	{
		$this->documentFiles->attach($documentFile);
	}

	/**
	 * Removes a DocumentFile
	 *
	 * @param \Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFileToRemove The DocumentFile to be removed
	 *
	 * @return void
	 */
	public function removeDocumentFile(\Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFileToRemove)
	{
		$this->documentFiles->detach($documentFileToRemove);
	}

	/**
	 * Remove all DocumentFiles
	 */
	public function removeAllDocumentFiles(){
		$this->initStorageObjects();
	}

	/**
	 * Returns the documentFiles
	 *
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile> $documentFiles
	 */
	public function getDocumentFiles()
	{
		return $this->documentFiles;
	}

	/**
	 * Sets the documentFiles
	 *
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\DocumentFile> $documentFiles
	 *
	 * @return void
	 */
	public function setDocumentFiles(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $documentFiles)
	{
		$this->documentFiles = $documentFiles;
	}

	/**
	 * Returns the practice
	 *
	 * @return \Artif\ArtifEqhm\Domain\Model\Practice $practice
	 */
	public function getPractice()
	{
		return $this->practice;
	}

	/**
	 * Sets the practice
	 *
	 * @param \Artif\ArtifEqhm\Domain\Model\Practice $practice
	 *
	 * @return void
	 */
	public function setPractice(\Artif\ArtifEqhm\Domain\Model\Practice $practice)
	{
		$this->practice = $practice;
	}

	/**
	 * Returns the basedOn
	 *
	 * @return \Artif\ArtifEqhm\Domain\Model\Document $basedOn
	 */
	public function getBasedOn()
	{
		return $this->basedOn;
	}

	/**
	 * Sets the basedOn
	 *
	 * @param \Artif\ArtifEqhm\Domain\Model\Document $basedOn
	 *
	 * @return void
	 */
	public function setBasedOn(\Artif\ArtifEqhm\Domain\Model\Document $basedOn = null)
	{
		$this->basedOn = $basedOn;
	}

	/**
	 * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category>
	 */
	public function getCategories()
	{
		return $this->categories;
	}

	/**
	 * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\Artif\ArtifEqhm\Domain\Model\Category> $categories
	 */
	public function setCategories(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories)
	{
		$this->categories = $categories;
	}

    /**
     * @param \Artif\ArtifEqhm\Domain\Model\Category $category
     *
     * @return void
     */
    public function addCategory(\Artif\ArtifEqhm\Domain\Model\Category $category)
    {
        $this->categories->attach($category);
    }

    /**
     * Returns the cloneOf
     *
     * @return \Artif\ArtifEqhm\Domain\Model\Document $cloneOf
     */
    public function getCloneOf()
    {
        return $this->cloneOf;
    }

    /**
     * Sets the cloneOf
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $cloneOf
     *
     * @return void
     */
    public function setCloneOf(\Artif\ArtifEqhm\Domain\Model\Document $cloneOf = null)
    {
        $this->cloneOf = $cloneOf;
    }

    /**
     * @return bool
     */
    public function isNotify(): bool
    {
        return $this->notify;
    }

    /**
     * @param bool $notify
     */
    public function setNotify(bool $notify)
    {
        $this->notify = $notify;
    }

    /**
     * @return bool
     */
    public function isGeneric(): bool
    {
        return $this->generic;
    }

    /**
     * @param bool $generic
     */
    public function setGeneric(bool $generic)
    {
        $this->generic = $generic;
    }

    /**
     * @return bool
     */
    public function getIsHygienePlan()
    {
        return $this->isHygienePlan;
    }

    /**
     * @param bool $isHygienePlan
     */
    public function setIsHygienePlan(bool $isHygienePlan)
    {
        $this->isHygienePlan = $isHygienePlan;
    }

    /**
     * @return bool
     */
    public function isDevice()
    {
        return $this->isDevice;
    }

    /**
     * @param bool $isDevice
     */
    public function setIsDevice($isDevice)
    {
        $this->isDevice = $isDevice;
    }
}
