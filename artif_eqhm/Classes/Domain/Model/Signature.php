<?php
namespace Artif\ArtifEqhm\Domain\Model;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * Signature
 */
class Signature extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * position
     *
     * @var string
     */
    protected $position = '';

    /**
     * signatureImage
     *
     * @var \TYPO3\CMS\Extbase\Domain\Model\FileReference
     * @cascade remove
     */
    protected $signatureImage = null;

    /**
     * signatureName
     *
     * @var string
     * @validate NotEmpty
     */
    protected $signatureName = '';

    /**
     * representation
     *
     * @var string
     */
    protected $representation = '';

    /**
     * functionOption
     *
     * @var string
     */
    protected $functionOption = '';

    /**
     * Returns the position
     *
     * @return string $position
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Sets the position
     *
     * @param string $position
     * @return void
     */
    public function setPosition($position)
    {
        $this->position = $position;
    }

    /**
     * Returns the signatureImage
     *
     * @return \TYPO3\CMS\Extbase\Domain\Model\FileReference $signatureImage
     */
    public function getSignatureImage()
    {
        return $this->signatureImage;
    }

    /**
     * Sets the signatureImage
     *
     * @param \TYPO3\CMS\Extbase\Domain\Model\FileReference $signatureImage
     * @return void
     */
    public function setSignatureImage(\TYPO3\CMS\Extbase\Domain\Model\FileReference $signatureImage = null)
    {
        $this->signatureImage = $signatureImage;
    }

    /**
     * Returns the signatureName
     *
     * @return string $signatureName
     */
    public function getSignatureName()
    {
        return $this->signatureName;
    }

    /**
     * Sets the signatureName
     *
     * @param string $signatureName
     * @return void
     */
    public function setSignatureName($signatureName)
    {
        $this->signatureName = $signatureName;
    }

    /**
     * @return string
     */
    public function getRepresentation ()
    {
        return $this->representation;
    }

    /**
     * @param string $representation
     */
    public function setRepresentation ($representation)
    {
        $this->representation = $representation;
    }

    /**
     * @return string
     */
    public function getFunctionOption ()
    {
        return $this->functionOption;
    }

    /**
     * @param string $functionOption
     */
    public function setFunctionOption ($functionOption)
    {
        $this->functionOption = $functionOption;
    }
}
