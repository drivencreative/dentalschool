<?php

namespace Artif\ArtifEqhm\Generators;

use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Service\DocumentGenerator;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class OrganigrammDocumentGenerator extends AbstractDocumentGenerator
{

    /**
     * @return array
     */
    public function generate(Practice $practice): array
    {
        $organigramm = $this->htmlToPdf($practice);

        $this->documentManager->createGenericNonEditableDocument(
            $practice,
            $organigramm
        );
        return $organigramm;
    }

    /**
     * @param Practice $practice
     * @return array
     */
    public function htmlToPdf(Practice $practice): array
    {
        $organigramm = [
            'organigramm' => $this->documentGenerator->htmlToPdf(
                'Organigramm' . $practice->getUid(),
                $this->documentGenerator->generateView(
                    $practice,
                    null,
                    $this->settings['pdf.']['organigrammGeneratedPdfFromHtml.']['template']
                ),
                [
                    'orientation' => 'landscape'
                ]
            )];
        return $organigramm;
    }
}