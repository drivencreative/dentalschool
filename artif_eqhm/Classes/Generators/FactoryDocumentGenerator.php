<?php

namespace Artif\ArtifEqhm\Generators;


use Artif\ArtifEqhm\Domain\Model\Practice;
use Exception;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class FactoryDocumentGenerator extends AbstractDocumentGenerator
{
    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface
     * @inject
     */
    protected $objectManager;

    public function generate($type = '', Practice $practice, $additionalDocPath='') {

        if($type == '') {
            throw new Exception('Invalid Document Type.');
        } else {
            $className = 'Artif\\ArtifEqhm\\Generators\\'.ucfirst($type).'DocumentGenerator';
            if(class_exists($className)){
                $generator = $this->objectManager->get($className);
                if($type=='QMHandbuch'){
                    return $generator->generate($practice, $additionalDocPath);
                }
                return $generator->generate($practice);
            } else {
                throw new Exception($className.' class not found.');
            }
        }
    }
}