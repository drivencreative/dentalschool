<?php
/**
 * Created by PhpStorm.
 * User: dradisic
 * Date: 10/19/17
 * Time: 3:42 PM
 */

namespace Artif\ArtifEqhm\Generators;


use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Service\DocumentGenerator;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class QMHandbuchDocumentGenerator extends AbstractDocumentGenerator
{
    /**
     * @param Practice $practice
     * @param $organigramm
     */
    public function generate(Practice $practice, $organigramm): void
    {
        $this->documentManager->createGenericNonEditableDocument(
            $practice,
            [
                'QMHandbuch' => $this->documentGenerator->append(
                    [
                        'A' => $this->storage->getConfiguration()['basePath'] . $this->generateCover($practice)['QMHandbuch'],
                        'B' => $this->storage->getConfiguration()['basePath'] . $organigramm['organigramm'],
                        'C' => $this->storage->getConfiguration()['basePath'] . $this->settings['pdf.']['qmhandbuchContent']
                    ],
                    'user_upload/QMHandbuchGenerate' . $practice->getUid() . '.pdf'
                )
            ]
        );
    }

    /**
     * @param Practice $practice
     * @return array
     */
    public function generateCover(Practice $practice): array
    {
        $qmHandbuchCover = [
            'QMHandbuch' => $this->documentGenerator->htmlToPdf(
                'QMHandbuchCover' . $practice->getUid(),
                $this->documentGenerator->generateView(
                    $practice,
                    null,
                    $this->settings['pdf.']['qmhandbuchGeneratedPdfFromHtml.']['template']
                ),
                [
                    'orientation' => 'landscape'
                ]
            )
        ];
        return $qmHandbuchCover;
    }
}