<?php

namespace Artif\ArtifEqhm\Generators;


use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class AbstractDocumentGenerator
{

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManager
     */
    protected $configurationManager;

    /**
     * @var array
     */
    protected $settings;

    /**
     * @var \Artif\ArtifEqhm\Domain\Model\Practice
     */
    protected $practice;

    /**
     * @var int
     */
    protected $storageUid = 1;

    /**
     * @var ResourceStorage
     */
    protected $storage;

    /**
     * @var \TYPO3\CMS\Core\Resource\ResourceFactory
     */
    protected $resourceFactory;

    /**
     * DocumentManager
     * @var \Artif\ArtifEqhm\Service\DocumentManager
     * @inject
     */
    protected $documentManager;

    /**
     * @var \Artif\ArtifEqhm\Service\DocumentGenerator
     * @inject
     */
    protected $documentGenerator = null;


    public function __construct()
    {
        /** @var \TYPO3\CMS\Core\Resource\ResourceFactory $resourceFactory */
        $this->resourceFactory = \TYPO3\CMS\Core\Resource\ResourceFactory::getInstance();

        /** @var ResourceStorage $storage */
        $this->storage = $this->resourceFactory->getStorageObject($this->storageUid);
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $this->configurationManager = $this->objectManager->get('TYPO3\CMS\Extbase\Configuration\ConfigurationManager');

        $extbaseFrameworkConfiguration = $this->configurationManager->getConfiguration(ConfigurationManagerInterface::CONFIGURATION_TYPE_FULL_TYPOSCRIPT);

        $this->settings = $extbaseFrameworkConfiguration['plugin.']['tx_artifeqhm_dm.']['settings.'];
    }
}