<?php

namespace Artif\ArtifEqhm\Generators;

use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Service\DocumentGenerator;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface;

class HygienePlanDocumentGenerator extends AbstractDocumentGenerator
{

    /**
     * @param Practice $practice
     */
    public function generate(Practice $practice): void
    {
        $this->documentManager->createGenericHygienePlanDocument(
            $practice,
            $this->documentGenerator->generateView(
                $practice,
                null,
                $this->settings['pdf.']['hygieneplanGeneratedPdfFromHtml.']['templateTable']
            ),
            $this->documentGenerator->generateView(
                $practice,
                null,
                $this->settings['pdf.']['hygieneplanGeneratedPdfFromHtml.']['templateCover']
            )
        );
    }
}