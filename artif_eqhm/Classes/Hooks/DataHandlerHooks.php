<?php

namespace Artif\ArtifEqhm\Hooks;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * Hooks for DataHandler
 */
class DataHandlerHooks
{
    public function processDatamap_afterDatabaseOperations($status, $table, $id, array $fieldArray, &$pObj)
    {
        if ($table === 'tx_artifeqhm_domain_model_terms') {
            if (in_array($status, ['update', 'new'])) {
                \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Database\ConnectionPool::class)
                    ->getQueryBuilderForTable('tx_artifeqhm_domain_model_practice')
                    ->update('tx_artifeqhm_domain_model_practice')
                    ->set('terms', 0)
                    ->execute();

//                /** @var $objectManager \TYPO3\CMS\Extbase\Object\ObjectManager **/
//                $objectManager = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\Object\ObjectManager::class);
//
//                /** @var $practiceRepository \Artif\ArtifEqhm\Domain\Repository\PracticeRepository **/
//                $practiceRepository = $objectManager->get(\Artif\ArtifEqhm\Domain\Repository\PracticeRepository::class);
//                $practiceRepository->setDefaultQuerySettings($objectManager->get(\TYPO3\CMS\Extbase\Persistence\Generic\Typo3QuerySettings::class)->setRespectStoragePage(FALSE));
//
//                /** @var $practices \TYPO3\CMS\Extbase\Persistence\Generic\QueryResult **/
//                $practices = $practiceRepository->findAll();
//
//                /** @var $practice \Artif\ArtifEqhm\Domain\Model\Practice **/
//                foreach ($practices as $practice) {
//                    $practice->setTerms(false);
//                    $practiceRepository->update($practice);
//                }
//                $practiceRepository->persistAll();
            }
        }
    }
}