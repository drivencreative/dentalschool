<?php

namespace Artif\ArtifEqhm\Utility;


use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class IteratorHandler
{
    /**
     * Recursively extract the key
     *
     * @param \Traversable $iterator
     * @param string $key
     * @return string
     * @throws \Exception
     */
    public static function recursivelyExtractKey($iterator, $key)
    {
        $content = [];

        foreach ($iterator as $v) {
            // Lets see if we find something directly:
            $result = ObjectAccess::getPropertyPath($v, $key);
            if (null !== $result) {
                $content[] = $result;
            } elseif (true === is_array($v) || true === $v instanceof \Traversable) {
                $content[] = self::recursivelyExtractKey($v, $key);
            }
        }

        $content = self::flattenArray($content);

        return $content;
    }

    /**
     * Flatten the result structure, to iterate it cleanly in fluid
     *
     * @param array $content
     * @param array $flattened
     * @return array
     */
    public static function flattenArray($content, $flattened = [])
    {
        if ($content) {
            foreach ($content as $sub) {
                if (method_exists($sub, 'toArray')) {
                    $sub = $sub->toArray();
                }
                if (true === is_array($sub)) {
                    $flattened = self::flattenArray($sub, $flattened);
                } else {
                    $flattened[] = $sub;
                }
            }
        }

        return $flattened;
    }

    /**
     * @param $record
     * @param $field
     * @return array
     */
    public static function getRecursiveUid($record, $field)
    {
        $result = [];
        if (method_exists($record, 'getUid')) {
            $result[] = $record->getUid();
            if ($record->_hasProperty($field) && $record->_getProperty($field)) {
                /* Workaround for loading relation instance -  skip if ObjectStorage */
                if (!$record->_getProperty($field) instanceof ObjectStorage) {
                    $record->_getProperty($field)->_getProperties();
                }
                $result = array_merge($result, self::getRecursiveUid($record->_getProperty($field), $field));
            }
        } else {
            foreach ($record as $item) {
                $result = array_merge($result, self::getRecursiveUid($item, $field));
            }
        }
        return $result;
    }
}