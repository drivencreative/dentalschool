<?php

namespace Artif\ArtifEqhm\Utility;

class NameGenerator
{

    /**
     * @param string $fileEnding
     * @param $suffix
     *
     * @return string
     */
    public static function generateFileName($fileEnding, $suffix){
        $tempPath = 'typo3temp/temp/';

        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < 12; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $tempPath.$randomString.$suffix.".$fileEnding";
    }

    /**
     * @param string $fileEnding
     * @param \Artif\ArtifEqhm\Domain\Model\DocumentFile $documentFile
     *
     * @return string
     */
    public static function getFileNamePath($fileEnding, $documentFile){
        $tempPath = 'typo3temp/temp/';

        $slugify = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\Cocur\Slugify\Slugify::class)->slugify($documentFile->getDocument()->getName());

        return $tempPath.$slugify.".$fileEnding";
    }
}