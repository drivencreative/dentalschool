<?php

namespace Artif\ArtifEqhm\Controller;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/
use Artif\ArtifEqhm\Domain\Model\Category;
use Artif\ArtifEqhm\Domain\Model\Document;
use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Domain\Model\Signature;
use Artif\ArtifEqhm\Domain\Repository\DocumentRepository;
use Artif\ArtifEqhm\Generators\FactoryDocumentGenerator;
use Artif\ArtifEqhm\Service\DocumentGenerator;
use mikehaertl\tmp\File;
use TYPO3\CMS\Core\Resource\FileInterface;
use TYPO3\CMS\Core\Resource\ResourceFactory;
use TYPO3\CMS\Core\Resource\ResourceInterface;
use TYPO3\CMS\Core\Resource\ResourceStorage;
use TYPO3\CMS\Core\Resource\StorageRepository;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

/**
 * SignatureController
 */
class PracticeController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
	/**
	 * FrontendUserRepository
	 *
	 * @var \Artif\ArtifEqhm\Domain\Repository\FrontendUserRepository
	 * @inject
	 */
	protected $frontendUserRepository = null;

	/**
	 * PracticeRepository
	 *
	 * @var \Artif\ArtifEqhm\Domain\Repository\PracticeRepository
	 * @inject
	 */
	protected $practiceRepository = null;

	/**
	 * @var \Artif\ArtifEqhm\Domain\Model\FrontendUser
	 */
	protected $currentUser = null;

	/**
	 * @var \Artif\ArtifEqhm\Domain\Model\Practice
	 */
	protected $practice;

    /**
     * @var \Artif\ArtifEqhm\Service\DocumentGenerator
     * @inject
     */
    protected $documentGenerator = null;

    /**
     * DocumentFileRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentFileRepository
     * @inject
     */
    protected $documentFileRepository = null;

    /**
     * DocumentManager
     * @var \Artif\ArtifEqhm\Service\DocumentManager
     * @inject
     */
    protected $documentManager;

    /**
     * CategoryRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * StaticDocumentGenerator
     *
     * @var \Artif\ArtifEqhm\Service\StaticDocumentGenerator
     * @inject
     */
    protected $staticDocumentGenerator;


    public function initializeAction()
    {
        $this->currentUser = $this->frontendUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        $this->practice = $this->practiceRepository->findByCurrentUser($this->currentUser);

        if (!$this->currentUser) {
            die('Not allowed to use!!!');
        }
        parent::initializeAction();
    }

    protected function getErrorFlashMessage()
    {
        return \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate('validation.fill_all_required_fields', $this->extensionName);
    }


    /**
     * @return void
     */
    public function editAction()
    {
        /** Get FlashMessage from the other Plugin */
        $this->addMessagesToDefaultQueue();

        if ($this->practice->getSignatures()->count() < 3) {
            $signatureToAdd = 3 - $this->practice->getSignatures()->count();
            for ($i = 0; $i < $signatureToAdd; $i++) {
                $this->practice->addSignature(new Signature());
            }
            $this->practiceRepository->update($this->practice);
            $this->practiceRepository->persistAll();
        }

        $this->view->assign('practice', $this->practice);
        $this->view->assign('terms', $this->objectManager->get(\Artif\ArtifEqhm\Domain\Repository\TermsRepository::class)->findLatest());
    }

    /**
     * @return void
     */
    public function initializeUpdateAction()
    {
        /** @var $configuration \TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration */
        $configuration = reset($this->arguments)->getPropertyMappingConfiguration();
        $args = $this->request->getArguments();
        foreach ($args['practice']['signatures'] as $key => $item) {
            $configuration->forProperty('signatures.' . $key . '.signatureImage')
                ->setTypeConverterOptions(
                    \Artif\ArtifEqhm\Domain\TypeConverter\FileReferenceConverter::class,
                    [
                        \Artif\ArtifEqhm\Domain\TypeConverter\FileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                        \Artif\ArtifEqhm\Domain\TypeConverter\FileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/EQHM/Signatures/',
                    ]
                );
        }
    }

    /**
     * action Update
     *
     * @param Practice $practice
     * @return void
     */
    public function updateAction(Practice $practice)
    {
        if ($practice->_isDirty('terms')) {
            $this->termsLog();
        }
        if($practice->getInitialized() || $practice->isMaster()) {
            $this->staticDocumentGenerator->generateStaticDocuments($practice);
        }
        $this->practiceRepository->update($practice);
        $this->practiceRepository->persistAll();

        $this->redirect('edit');
    }

    /**
     * termsLog
     */
    protected function termsLog()
    {
        /** @var \Artif\ArtifEqhm\Domain\Model\TermsLog $termsLog * */
        $termsLog = $this->objectManager->get(\Artif\ArtifEqhm\Domain\Model\TermsLog::class);
        $termsLog->setPractice($this->practice);
        $termsLog->setFrontendUser($this->currentUser);
//        $termsLog->setTerms($this->objectManager->get(\Artif\ArtifEqhm\Domain\Repository\TermsRepository::class)->findLatest());

        if (!$termsLog->getTerms()) {
            $termsLog->setTerms($this->objectManager->get(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class));
        }
        $termsLog->getTerms()->setOriginalResource($this->objectManager->get(\Artif\ArtifEqhm\Domain\Repository\TermsRepository::class)->findLatest()->getDocument()->getOriginalResource());

        $this->objectManager->get(\Artif\ArtifEqhm\Domain\Repository\TermsLogRepository::class)->add($termsLog);
        $this->practiceRepository->persistAll();
    }

    /**
     * addMessagesToDefaultQueue
     */
    protected function addMessagesToDefaultQueue()
    {
        $queue = $this->controllerContext->getFlashMessageQueue('extbase.flashmessages.tx_artifeqhm_dm');
        $msg = $queue->getAllMessagesAndFlush();
        if ($msg) {
            $defaultQueue = $this->controllerContext->getFlashMessageQueue();
            foreach ($msg as $m) {
                $defaultQueue->enqueue($m);
            }
        }
    }
}
