<?php

namespace Artif\ArtifEqhm\Controller;

use Artif\ArtifEqhm\Domain\Model\Category;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

class DocumentAjaxController extends DocumentController
{
    /**
     *  action initialize
     */
    public function initializeAction()
    {
        parent::initializeAction();
    }

    /**
     * action getDocumentsByCategory
     * @param Category $category
     * @return void
     * @throws Exception
     * @ignorevalidation $category
     */
    public function getDocumentsByCategoryAction(Category $category)
    {

        $args = $this->request->getArguments();

        $this->unsigned = $this->editor && $args['pending'];
        $this->categories = $this->categoryRepository->findByParent(0);
        $this->documents = $this->documentRepository->findByPractice($this->practice)->toArray();

        if (is_array($this->documents)) {
            $this->userCategories = $this->getAllCategoryUids($this->getUserCategories($this->documents));
        } else {
            $this->userCategories = $this->getAllCategoryUids($this->categoryRepository->findAll()->toArray());
        }

        // @TODO Not allowed view
        if (!$this->currentUser) {
            throw new  Exception('Not allowed to use!!!');
        }
        $documents = $this->documentRepository->findByCategoryAndPractice($category->getUid(), $this->practice->getUid());
        $this->view->assign('pending', $args['pending']);
        $this->view->assign('editor', $this->editor);
        $this->view->assign('args', $args);
        $this->view->assign('categories', $this->categories);
        $this->view->assign('userCategories', $this->userCategories);
        $this->view->assign('documents', $documents);
        $this->view->assign('category', $category);
    }
}