<?php

namespace Artif\ArtifEqhm\Controller;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/
use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\Signature;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use TYPO3\CMS\Frontend\Exception;

/**
 * SignatureController
 */
class SignatureController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{

	/**
	 * documentRepository
	 *
	 * @var \Artif\ArtifEqhm\Domain\Repository\DocumentRepository
	 * @inject
	 */
	protected $documentRepository = null;

	/**
	 * DocumentFileRepository
	 *
	 * @var \Artif\ArtifEqhm\Domain\Repository\DocumentFileRepository
	 * @inject
	 */
	protected $documentFileRepository = null;

	/**
	 * FrontendUserRepository
	 *
	 * @var \Artif\ArtifEqhm\Domain\Repository\FrontendUserRepository
	 * @inject
	 */
	protected $frontendUserRepository = null;

	/**
	 * PracticeRepository
	 *
	 * @var \Artif\ArtifEqhm\Domain\Repository\PracticeRepository
	 * @inject
	 */
	protected $practiceRepository = null;

	/**
	 * documentRepository
	 *
	 * @var \Artif\ArtifEqhm\Service\DataHandler
	 * @inject
	 */
	protected $dataHandler = null;

	/**
	 * PortableDocumentFormatGenerator
	 *
	 * @var \Artif\ArtifEqhm\Service\PortableDocumentFormatGenerator
	 * @inject
	 */
	protected $portableDocumentFormatGenerator = null;

	/**
	 * TableGenerator
	 *
	 * @var \Artif\ArtifEqhm\Service\TableGenerator
	 * @inject
	 */
	protected $tableGenerator = null;

	/**
	 * @var \Artif\ArtifEqhm\Domain\Model\FrontendUser
	 */
	protected $currentUser = null;

	/**
	 * @var \Artif\ArtifEqhm\Domain\Model\Practice
	 */
	protected $practice;


	public function initializeAction()
	{
		$this->currentUser = $this->frontendUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
		$this->practice = $this->practiceRepository->findByFrontendUser($this->currentUser)->getFirst();

		/** Init all Services */
		$this->portableDocumentFormatGenerator->initService($this->practice);
		$this->tableGenerator->initService($this->practice);

		if (!$this->currentUser) {
			die('Not allowed to use!!!');
		}
		parent::initializeAction();
	}

	/**
	 * @param \Artif\ArtifEqhm\Domain\Model\Document $document
	 */
	public function showVersionForSigningAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
	{
		$this->view->assign('document',$document);
		$this->view->assign('documentFile',$document->getNewestVersionOfDocumentFile());
		$this->view->assign('practice', $this->practice);
	}

    /**
     * @param DocumentFile $documentFile
     * @param Signature $signature
     * @throws Exception
     */
	public function signDocumentAction(DocumentFile $documentFile, Signature $signature = null){
	    if (!$signature) {
            $this->addFlashMessage(
                LocalizationUtility::translate('validation.fill_all_required_fields',$this->extensionName),
                '',
                FlashMessage::ERROR
            );
	        $this->redirect('showVersionForSigning', null, null, ['document' => $documentFile->getDocument()->getUid()]);
        }
        $documentFile->setSigned(new \DateTime());
        $documentFile->setSignatureName($signature->getSignatureName());

        // Set Practice signature index for using the correct label
        foreach ($this->practice->getSignatures()->toArray() as $key => $practiceSignature){
            if($practiceSignature->getUid() == $signature->getUid()){
                $documentFile->setSignatureIndex($key);
            }
        }


        if ($signature->getSignatureImage() && $signature->getSignatureImage()->getOriginalResource()) {
            if (!$documentFile->getSignatureImage()) {
                $documentFile->setSignatureImage($this->objectManager->get(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class));
            }
            $documentFile->getSignatureImage()->setOriginalResource($signature->getSignatureImage()->getOriginalResource());
        }

        $this->documentFileRepository->update($documentFile);
        $this->documentFileRepository->persistAll();

        $this->redirect('list', 'Document');
	}

}
