<?php

namespace Artif\ArtifEqhm\Controller;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

/**
 * DeviceController
 */
class DeviceController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * DeviceRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DeviceRepository
     * @inject
     */
    protected $deviceRepository = null;

    /**
     * DocumentRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentRepository
     * @inject
     */
    protected $documentRepository = null;

    /**
     * DocumentFileRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentFileRepository
     * @inject
     */
    protected $documentFileRepository = null;

    /**
     * CategoryRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * DeviceManager
     *
     * @var \Artif\ArtifEqhm\Service\DeviceManager
     * @inject
     */
    protected $deviceManager;

    /**
     * Document manager
     * @var \Artif\ArtifEqhm\Service\DocumentManager
     * @inject
     */
    protected $documentManager;

    /**
     * FrontendUserRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository = null;

    /**
     * PracticeRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\PracticeRepository
     * @inject
     */
    protected $practiceRepository = null;

    /**
     * @var \Artif\ArtifEqhm\Domain\Model\Practice
     */
    protected $practice;

    /**
     * @var \Artif\ArtifEqhm\Domain\Model\FrontendUser
     */
    protected $currentUser = null;

    /**
     * @var boolean
     */
    protected $editor = false;

    /**
     *  action initialize
     */
    public function initializeAction()
    {
        if (!empty(array_intersect(explode(',', $this->settings['adminGroup']), $GLOBALS['TSFE']->fe_user->groupData['uid']))) {
            $this->editor = true;
        }

        $this->currentUser = $this->frontendUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        $this->practice = $this->practiceRepository->findByCurrentUser($this->currentUser);

        parent::initializeAction();
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        if (!$this->practice->isMaster() && !$this->currentUser->isReadOnly($this->settings['readOnlyGroup']) && !$this->deviceRepository->countByPractice($this->practice)) {
            $this->redirect('clone');
        }

        $args = $this->request->getArguments();
//        $devices = $args['category'] ? $this->deviceRepository->findByPracticeAndCategory($this->practice, $args['category']) : $this->deviceRepository->findByPractice($this->practice);
        $devices = null;
        if ($args['category']) {
            $devices =  $this->deviceRepository->findByPracticeAndCategory($this->practice, $args['category']);
        }

        $params = [
            'editor' => $this->editor,
            'devices' => $devices,
            'category' => $args['category'],
            'categories' => $this->categoryRepository->findByParent($this->settings['category']),
        ];
        // Ajax
        if (!isset($this->configurationManager->getContentObject()->data['CType'])) {
            return $this->view->renderPartial(
                'Device/List',
                null,
                $params
            );
        }

        $this->view->assignMultiple($params);
    }

    /**
     * action show
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Device $device
     *
     * @return void
     */
    public function showAction(\Artif\ArtifEqhm\Domain\Model\Device $device)
    {
        $this->view->assign('editor', $this->editor);
        $this->view->assign('device', $device);
    }

    /**
     * action new
     */
    public function newAction()
    {
        if ($this->request->hasArgument('category')) {
            $this->view->assign('category', $this->request->getArgument('category'));
        }
    }

    /**
     * create update
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Device $device
     * @return void
     */
    public function createAction(\Artif\ArtifEqhm\Domain\Model\Device $device)
    {
        $device->setPractice($this->practice);

        $this->deviceRepository->add($device);
        $this->persistenceManager->persistAll();
        $this->createDocument($device->getCategories());
        $this->redirect('list', null, null, $this->currentCategory());
    }

    /**
     * action edit
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Device $device
     * @return void
     */
    public function editAction(\Artif\ArtifEqhm\Domain\Model\Device $device)
    {
        $this->view->assign('editor', $this->editor);
        $this->view->assign('device', $device);
    }

    /**
     * action update
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Device $device
     */
    public function updateAction(\Artif\ArtifEqhm\Domain\Model\Device $device)
    {
        // $this->persistenceManager->persistAll();
        $this->deviceRepository->update($device);
        $this->createDocument($device->getCategories());
        $this->redirect('list', null, null, $this->currentCategory());
    }

    /**
     * action clone
     */
    public function cloneAction()
    {
        if ($this->deviceRepository->findByPractice($this->practice)->count()) {
            $this->redirect('list');
        }
        $this->view->assign('categories', $this->categoryRepository->findByParent($this->settings['category']));
    }

    /**
     *  action makeClone
     */
    public function makeCloneAction()
    {
        /** @var \Artif\ArtifEqhm\Domain\Model\Practice $master */
        $master = $this->practiceRepository->findByMaster(true)->getFirst();

        if ($this->request->getArguments()['selectedCategories']) {
            $devices = $this->deviceRepository->findByCategoryAndMaster($this->request->getArguments()['selectedCategories']);
        } else {
            $devices = $this->deviceRepository->findByPractice($master);
        }

        if ($devices) {
            $categories = [];
            $propertyMapper = $this->objectManager->get(\TYPO3\CMS\Extbase\Property\PropertyMapper::class);
            foreach ($devices as $device) {
                $property = $device->_getProperties();
                unset($property['uid'], $property['pid'], $property['practice'], $property['categories']);
                /** @var \Artif\ArtifEqhm\Domain\Model\Device $clone **/
                $clone = $propertyMapper->convert($property, \Artif\ArtifEqhm\Domain\Model\Device::class);
                // $clone = $this->objectManager->get(\Artif\ArtifEqhm\Domain\Model\Device::class);
                $clone->setPractice($this->practice);
                foreach ($device->getCategories() as $category) {
                    $clone->addCategory($category);
                    $categories[$category->getUid()] = $category;
                }
                $this->deviceRepository->add($clone);
            }
            $this->persistenceManager->persistAll();

            if ($categories) {
                $this->createDocument($categories);
            }
        }

        $this->redirect('list');
    }

    /**
     * @return array
     */
    public function currentCategory()
    {
        $args = $this->request->getArguments();
        if ($args['device']['categories'][0]) {
            return ['category' => $args['device']['categories'][0]];
        }
        return [];
    }

    /**
     * @param array|\TYPO3\CMS\Extbase\Persistence\ObjectStorage $categories
     */
    public function createDocument($categories): void
    {
        if ($categories) {
            /** @var \Artif\ArtifEqhm\Domain\Model\Category $category */
            foreach ($categories as $category) {
                /** @var \Artif\ArtifEqhm\Domain\Model\Document $categoryDocument */
                $categoryDocument = $this->documentRepository->findByCategoryAndIsDevice($this->practice, [$category->getUid()]);
                if ($categoryDocument->count()) {
                    /** @var $documentFile $documentFiles */
                    $documentFile = $categoryDocument->getFirst()->getDocumentFiles()->toArray()[0];
                    // Update Content with all Device Records
                    $documentFile->setContent($this->deviceManager->generateTemplate($this->practice, $category));
                    $this->documentFileRepository->update($documentFile);
                } else {
                    // Create new document File
                    $document = $this->deviceManager->generateDocument($this->practice, $category);
                    if ($document instanceof \Artif\ArtifEqhm\Domain\Model\Document) {
                        $document->setIsDevice(true);
                        $document->addCategory($category);
                        $document->setTyp('DeviceList');
                        $this->documentRepository->add($document);
                        $this->persistenceManager->persistAll();
                    }
                }
            }
        }
    }

}
