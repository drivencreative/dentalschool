<?php

namespace Artif\ArtifEqhm\Controller;

/***
 *
 * This file is part of the "artif eqhm" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Markus Döll <markus.doell@artif.com>, artif GmbH & Co. KG
 *
 ***/

use Artif\ArtifEqhm\Domain\Model\Document;
use Artif\ArtifEqhm\Domain\Model\DocumentFile;
use Artif\ArtifEqhm\Domain\Model\DownloadLog;
use Artif\ArtifEqhm\Domain\TypeConverter\FileReferenceConverter;
use Artif\ArtifEqhm\Service\HeaderAndFooterGenerator;
use Artif\ArtifEqhm\Utility\IteratorHandler;
use TYPO3\CMS\Core\Exception;
use TYPO3\CMS\Core\Messaging\FlashMessage;
use TYPO3\CMS\Extbase\Domain\Model\Category;
use TYPO3\CMS\Extbase\Mvc\Exception\InvalidArgumentValueException;
use TYPO3\CMS\Extbase\Mvc\View\ViewInterface;
use TYPO3\CMS\Extbase\Persistence\Generic\QueryResult;
use TYPO3\CMS\Extbase\Utility\LocalizationUtility;
use Artif\ArtifEqhm\Domain\Model\Practice;
use Artif\ArtifEqhm\Domain\Model\FrontendUser;
use TYPO3\CMS\Core\Configuration\Richtext;
use TYPO3\CMS\Core\Resource\File;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Repository\CategoryRepository;
use TYPO3\CMS\Extbase\Persistence\ObjectStorage;
use TYPO3\CMS\Extbase\Reflection\ObjectAccess;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use TYPO3\CMS\Lang\Service\TranslationService;
use TYPO3\CMS\RteCKEditor\Form\Element\RichTextElement;

/**
 * DocumentController
 */
class DocumentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * @var \Artif\ArtifEqhm\Service\HeaderAndFooterGenerator
     * @inject
     */
    protected $headerAndFooterGenerator;

    /**
     * DocumentRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentRepository
     * @inject
     */
    protected $documentRepository = null;

    /**
     * DocumentFileRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\DocumentFileRepository
     * @inject
     */
    protected $documentFileRepository = null;

    /**
     * FrontendUserRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\FrontendUserRepository
     * @inject
     */
    protected $frontendUserRepository = null;

    /**
     * PracticeRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\PracticeRepository
     * @inject
     */
    protected $practiceRepository = null;

    /**
     * CategoryRepository
     *
     * @var \Artif\ArtifEqhm\Domain\Repository\CategoryRepository
     * @inject
     */
    protected $categoryRepository;


    /**
     * PortableDocumentFormatGenerator
     *
     * @var \Artif\ArtifEqhm\Service\PortableDocumentFormatGenerator
     * @inject
     */
    protected $portableDocumentFormatGenerator = null;

    /**
     * TableGenerator
     *
     * @var \Artif\ArtifEqhm\Service\TableGenerator
     * @inject
     */
    protected $tableGenerator = null;

    /**
     * DeviceListGenerator
     *
     * @var \Artif\ArtifEqhm\Service\DeviceListGenerator
     * @inject
     */
    protected $deviceListGenerator = null;

    /**
     * NoneEditablePortableDocumentFormatGenerator
     *
     * @var \Artif\ArtifEqhm\Service\NoneEditablePortableDocumentFormatGenerator
     * @inject
     */
    protected $noneEditablePortableDocumentFormatGenerator = null;

    /**
     * LinkGenerator
     *
     * @var \Artif\ArtifEqhm\Service\LinkGenerator
     * @inject
     */
    protected $linkGenerator = null;

    /**
     * @var \Artif\ArtifEqhm\Domain\Model\FrontendUser
     */
    protected $currentUser = null;

    /**
     * @var \Artif\ArtifEqhm\Domain\Model\Practice
     */
    protected $practice;

    /**
     * @var \TYPO3\CMS\Extbase\Persistence\Generic\PersistenceManager
     * @inject
     */
    protected $persistenceManager;

    /**
     * @var boolean
     */
    protected $editor = false;

    /**
     * @var boolean
     */
    protected $unsigned = false;

    /**
     * @var array
     */
    protected $userCategories = [];

    /**
     * @var QueryResult
     */
    protected $categories = null;

    /**
     * @var array
     */
    protected $documents;

    /**
     * configuration
     *
     * @var array
     */
    protected $configuration = [];

    /**
     * DocumentManager
     * @var \Artif\ArtifEqhm\Service\DocumentManager
     * @inject
     */
    protected $documentManager;

    /**
     * @var \Artif\ArtifEqhm\Service\DocumentGenerator
     * @inject
     */
    protected $documentGenerator = null;

    /**
     * StaticDocumentGenerator
     *
     * @var \Artif\ArtifEqhm\Service\StaticDocumentGenerator
     * @inject
     */
    protected $staticDocumentGenerator = null;

    /**
     *  action initialize
     */
    public function initializeAction()
    {
        if (!empty(array_intersect(explode(',', $this->settings['adminGroup']), $GLOBALS['TSFE']->fe_user->groupData['uid']))) {
            $this->editor = true;
        } elseif (in_array($this->request->getControllerActionName(), ['edit', 'newVersion'])) {
            $this->redirect('list');
        }
        $this->currentUser = $this->frontendUserRepository->findByUid($GLOBALS['TSFE']->fe_user->user['uid']);
        $this->practice = $this->practiceRepository->findByCurrentUser($this->currentUser);
        if (!$this->practice) {
            throw new \TYPO3\CMS\Extbase\Exception("No practice for user Uid: " . $this->currentUser->getUid());
        }
        /** Init all Services */
        $this->portableDocumentFormatGenerator->initService($this->practice);
        $this->tableGenerator->initService($this->practice);
        $this->deviceListGenerator->initService($this->practice);
        $this->noneEditablePortableDocumentFormatGenerator->initService($this->practice);
        $this->linkGenerator->initService($this->practice);

        parent::initializeAction();
    }


    /**
     * @param ViewInterface $view
     */
    public function initializeView(ViewInterface $view)
    {
        parent::initializeView($view);
    }

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $this->generateList();

    }

    /**
     * action notifications
     *
     * @return void
     */
    public function notificationsAction()
    {
        $this->generateList();

    }

    /**
     * action pending
     *
     * @return void
     */
    public function pendingAction()
    {
        $this->documents = $this->documentRepository->findPending($this->practice)->toArray();
        $this->generateList();
    }

    /**
     * @param $categories
     * @return array
     */
    protected function getAllCategoryUids($categories)
    {
        $result = array_merge([], IteratorHandler::getRecursiveUid($categories, 'parent'), IteratorHandler::getRecursiveUid($categories, 'categories'));
        return array_unique($result);
    }

    /**
     * @param $documents
     * @return array
     */
    protected function getUserCategories($documents)
    {

//        DebuggerUtility::var_dump(IteratorHandler::recursivelyExtractKey($documents, 'categories'));die;
        return array_unique(IteratorHandler::flattenArray(IteratorHandler::recursivelyExtractKey($documents, 'categories')));
    }

    /**
     * action cloneFiles
     */
    public function cloneFilesAction()
    {
        if ($this->documentRepository->findByPractice($this->practice)->count()) {
            $this->redirect('list');
        }
        $rootLine = $this->categoryRepository->findByParent(0);
        $this->view->assign('categories', $rootLine);
    }

    /**
     *  action makeCloneDocuments
     */
    public function makeCloneDocumentsAction()
    {
        /** @var Practice $master */
        $master = $this->practiceRepository->findByMaster(true)->getFirst();

        $documents = $this->documentRepository->findAllByPractice($master);
        if ($this->request->getArguments()['selectedCategories']) {
            $documents = $this->documentRepository->findByCategoryAndMaster($this->request->getArguments()['selectedCategories']);
        }

        if ($documents) {
            foreach ($documents as $document) {
                $this->documentManager->createNewCloneDocument($document, $this->practice);
            }
        }
        $this->staticDocumentGenerator->generateStaticDocuments($this->practice);
        $this->practice->setInitialized(true);
        $this->practiceRepository->update($this->practice);
        $this->redirect('list');
    }

    /**
     * action show
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     *
     * @return void
     */
    public function showAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        $this->view->assign('document', $document);
    }

    /**
     * action newVersion
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     */
    public function newVersionAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        $this->view->assign('documentFile', $document->getNewestVersionOfDocumentFile());
        $this->view->assign('document', $document);
    }

    /**
     * action generatePDF
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     * @return void
     */
    public function generatePDFAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        // @TODO Test pdf generator for all file types including html tables

        $documentFile = $this->editor ? $document->getNewestVersionOfDocumentFile() : $document->getNewestVersionOfSignedDocumentFile();
        if ($documentFile) {
            $file = $this->{lcfirst($document->getTyp()) . 'Generator'}->generatePDF($documentFile);
            if (is_file($file)) {
                $this->downloadLog($documentFile);

                header('Content-Description: File Transfer');
                header('Content-Type: ' . mime_content_type($file));
                header('Content-Disposition: attachment; filename="' . basename($file).'"');
                header('Content-Transfer-Encoding: binary');
                header('Expires: 0');
                header('Cache-Control: public, must-revalidate, max-age=0');
                header('Pragma: public');
                header('Content-Length: ' . filesize($file));
                ob_clean();
                flush();
                readfile($file);exit;
            } else {
                die($file);
            }
        } else {
            $this->redirect('list');
        }
    }

    /**
     * action edit
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     * @ignorevalidation $document
     *
     * @return void
     */
    public function editAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        $this->view->assign('documentFile', $document->getNewestVersionOfDocumentFile());
        $this->view->assign('document', $document);
    }

    /**
     * Initialize Create Action
     */
    public function initializeCreateAction()
    {
        /** @var $configuration \TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration */
        $configuration = reset($this->arguments)->getPropertyMappingConfiguration();
        $configuration
            ->forProperty('documentFiles.0.file')
            ->skipUnknownProperties()
            ->allowAllProperties()
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                true
            );
    }

    /**
     * create update
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     * @return void
     */
    public function createAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        $args = $this->request->getArguments();
        if ($args['document']['categories'][0]['__identity']) {
            $category = $this->categoryRepository->findByUid($args['document']['categories'][0]['__identity']);
            $document->addCategory($category);
        }

        if (!$document->getTyp()) {
            $document->setTyp('Table');
        }

        // When Name is empty use from document file for type RteDocument and Upload
        if (!$document->getName() && $document->getDocumentFiles()) {
            $documentFile = $document->getDocumentFiles()->toArray()[0];
            $document->setName(strip_tags($documentFile->getContent()) ?: $documentFile->getFile()->getOriginalResource()->getName());
        }

        $document->setBasedOnDocumentVersion('0');
        $document->setPractice($this->practice);

        $this->documentRepository->add($document);
        $this->persistenceManager->persistAll();

        $this->redirect('list', null, null, $this->currentCategory());
    }

    /**
     * Initialize Update Action
     */
    public function initializeUpdateAction()
    {
        $this->configuration = $this->arguments['documentFile']->getPropertyMappingConfiguration()
            ->allowAllProperties()
            ->skipUnknownProperties(true)
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                true
            );
    }


    /**
     * action update
     *
     * @param Document $document
     * @param DocumentFile $documentFile
     */
    public function updateAction(Document $document, DocumentFile $documentFile = null)
    {
        if ($documentFile) {
            $documentFileToUpdate = $document->getNewestVersionOfDocumentFile();
            $documentFileToUpdate->setContent($documentFile->getContent());
        }

        $this->documentRepository->update($document);
//        $this->persistenceManager->persistAll();

        $this->redirect('list', null, null, $this->currentCategory());
    }

    /**
     * @param Document $document
     * @throws InvalidArgumentValueException
     */
    public function compareWithBaseAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        $this->view->assign('document', $document);
        $signedDocument = $document->getNewestVersionOfSignedDocumentFile() ?? $document->getNewestVersionOfDocumentFile();
        if (!$signedDocument instanceof DocumentFile) {
            throw new InvalidArgumentValueException('DocumentFile is missing in Document ' . $document->getUid());
        }
        $this->view->assign('documentFile', $signedDocument);
        $this->view->assign('documentCloneOf', $document->getCloneOf());
    }

    /**
     * Initialize UpdateCompare Action
     */
    public function initializeUpdateCompareAction()
    {
        $this->configuration = $this->arguments['documentFileTarget']->getPropertyMappingConfiguration()
            ->allowAllProperties()
            ->skipUnknownProperties(true)
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                true
            );
    }

    /**
     * action updateCompare
     *
     * @param Document $document
     * @param DocumentFile $documentFileParent
     * @param DocumentFile $documentFileTarget
     * @return void
     */
    public function updateCompareAction(Document $document, DocumentFile $documentFileParent = null, DocumentFile $documentFileTarget = null)
    {
        if ($this->request->hasArgument('btn1')) {
            $document->setName($document->getChangedName());
            $document->setNotify(false);

            if ($documentFileParent && $documentFileTarget) {
                $document->setBasedOnDocumentVersion($documentFileParent->getVersion());
                $documentFileTarget->setContent($documentFileParent->getContent());
                $documentFileTarget->setAdditionalContent($documentFileParent->getAdditionalContent());
                if ($documentFileParent->getFile()) $documentFileTarget->setFile($documentFileParent->getFile());
                $documentFileTarget->setSigned($documentFileParent->getSigned());
                $documentFileTarget->setSignatureName($documentFileParent->getSignatureName());
                $documentFileTarget->setSignatureImageFileReference($documentFileParent);
            }
        }
        if ($this->request->hasArgument('btn2')) {
            $document->setNotify(false);
        }

        $this->documentRepository->update($document);
        if ($documentFileTarget) {
            $this->documentFileRepository->update($documentFileTarget);
        }

        $this->redirect('list', null, null, $this->currentCategory());
    }

    /**
     *  action updateNewVersion
     *
     * @param Document $document
     * @param DocumentFile $documentFile
     */
    public function updateNewVersionAction(Document $document, DocumentFile $documentFile = null)
    {
        $documentFile->setVersion(
            $document
                ->getNewestVersionOfDocumentFile()
                ->getVersion() + 1
        );
        if ($document->getBasedOn()) {
            if ($document->getBasedOnNewestVersionOfDocumentFile()->getFile()) {
                $documentFile->setFile(
                    $document
                        ->getBasedOnNewestVersionOfDocumentFile()
                        ->getFile()
                );
            }
            $documentFile->setContent(
                $document
                    ->getBasedOnNewestVersionOfDocumentFile()
                    ->getContent()
            );
            $documentFile->setAdditionalContent(
                $document
                    ->getBasedOnNewestVersionOfDocumentFile()
                    ->getAdditionalContent()
            );
            $document->setBasedOnDocumentVersion(
                $document
                    ->getBasedOn()
                    ->getNewestVersionOfDocumentFile()
                    ->getVersion()
            );
        }

        $document->addDocumentFile($documentFile);
        $this->setClonesOf($document);
        $this->documentRepository->update($document);
        $this->redirect('list');
    }

    /**
     * Initialize UpdateCompare Action
     */
    public function initializeNewAction()
    {
        $this->configuration = reset($this->arguments)->getPropertyMappingConfiguration()
            ->allowAllProperties()
            ->skipUnknownProperties(true)
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
                true
            )
            ->setTypeConverterOption(
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::class,
                \TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
                true
            );
    }

    /**
     * action new
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     *
     * @return void
     */
    public function newAction(\Artif\ArtifEqhm\Domain\Model\Document $document = null)
    {
        $categoryUid = $this->request->getArguments()['category'];
        if ($categoryUid) $category = $this->categoryRepository->findByUid($categoryUid);

        $this->view->assign('document', $document);
        $this->view->assign('category', $category);
    }

    /**
     * initializeUpload action
     */
    public function initializeUploadAction()
    {
        /** @var $configuration \TYPO3\CMS\Extbase\Property\PropertyMappingConfiguration */
        $configuration = reset($this->arguments)->getPropertyMappingConfiguration();
        $configuration
            ->forProperty('documentFiles.0.file')
            ->skipUnknownProperties()
            ->setTypeConverterOptions(
                FileReferenceConverter::class,
                [
                    FileReferenceConverter::CONFIGURATION_ALLOWED_FILE_EXTENSIONS => $GLOBALS['TYPO3_CONF_VARS']['GFX']['imagefile_ext'],
                    FileReferenceConverter::CONFIGURATION_UPLOAD_FOLDER => '1:/user_upload/',
                ]
            );
    }

    /**
     * action upload
     *
     * @param \Artif\ArtifEqhm\Domain\Model\Document $document
     */
    public function uploadAction(\Artif\ArtifEqhm\Domain\Model\Document $document)
    {
        foreach ($document->getDocumentFiles() as $documentFile) {
            if (!$documentFile->getFile()) {
                $documentFile->setFile($this->objectManager->get(\TYPO3\CMS\Extbase\Domain\Model\FileReference::class));
            }
            $documentFile->getFile()->setOriginalResource($documentFile->getFile()->getOriginalResource());
        }
        $args = $this->request->getArguments();
        if ($args['document']['categories'][0]['__identity']) {
            $category = $this->categoryRepository->findByUid($args['document']['categories'][0]['__identity']);
            $document->addCategory($category);
        }
        $document->setName(basename(current($document->getDocumentFiles()->toArray())->getFile()->getOriginalResource()->getPublicUrl()));
        $document->setTyp('NoneEditablePortableDocumentFormat');
        $document->setBasedOnDocumentVersion('0');
        $document->setPractice($this->practice);
        $this->documentRepository->add($document);
        $this->persistenceManager->persistAll();
        $this->redirect('list', null, null, $this->currentCategory());
    }

    /**
     * @param Document $document
     */
    public function setClonesOf(\Artif\ArtifEqhm\Domain\Model\Document $document): void
    {
        $this->documentRepository->updateAllClonesOf($document);
    }

    /**
     * generate Document list for actions
     */
    public function generateList(): void
    {
        $args = $this->request->getArguments();

        $this->categories = $this->categories ?: $this->categoryRepository->findByParent(0);
        $this->documents = $this->documents ?: $this->documentRepository->findByPractice($this->practice)->toArray();
//        if($this->request->getArguments()['category']){
//            $this->documents = $this->documentRepository->findByCategoryAndPractice($this->request->getArgument('category'), $this->practice->getUid())->toArray();
//        }
        if (is_array($this->documents)) {
            $this->userCategories = $this->getAllCategoryUids($this->getUserCategories($this->documents));
        } else {
            $this->userCategories = $this->userCategories ?: $this->getAllCategoryUids($this->categoryRepository->findAll()->toArray());
        }

        // @TODO Not allowed view
        if (!$this->currentUser) {
            throw new Exception('Not allowed to use!!!');
        }

        /** Check if Practice Full Filled */
        $this->practiceCheck();
        if (!$this->practice->isMaster() && !$this->currentUser->isReadOnly($this->settings['readOnlyGroup']) && !$this->documentRepository->findByPractice($this->practice)->count()) {
            $this->redirect('cloneFiles');
        }
        $this->view->assign('isMaster', $this->practice->isMaster());
        $this->view->assign('practice', $this->practice);
        $this->view->assign('pending', $args['pending']);
        $this->view->assign('editor', $this->editor);
        $this->view->assign('args', $args);
        $this->view->assign('categories', $this->categories);
        $this->view->assign('userCategories', $this->userCategories);
        $this->view->assign('documents', $this->documents);
        $this->view->assign('notifications', $this->documentRepository->findAllNotify($this->practice));
        $this->view->assign('category', (int)$args['category']);
    }

    /**
     * @return array
     */
    public function currentCategory(): array
    {
        $args = $this->request->getArguments();
        if ($args['document']['categories'][0]['__identity']) {
            $category = $this->categoryRepository->findByUid($args['document']['categories'][0]['__identity']);
            return ['category' => $category->getUid()];
        }
        return [];
    }

    /**
     * Check if Practice Full Filled
     */
    protected function practiceCheck() {
        $validator = $this->practice->validation();
        if ($validator->hasErrors()) {
            $errors = $validator->getFlattenedErrors();

            $message = '';
            if (!$errors['terms'] || count($errors) > 1) {
                $message = LocalizationUtility::translate('fillPracticeBody', $this->extensionName) . PHP_EOL;
            }
            if ($errors['terms']) {
                $message .= LocalizationUtility::translate('fillPracticeTerms', $this->extensionName);
            }

            $this->addFlashMessage(
                $message,
                LocalizationUtility::translate('fillPracticeTitle', $this->extensionName),
                FlashMessage::ERROR
            );
            $this->redirect('edit', 'Practice', null, null, $this->settings['practiceEditUid']);
        }
    }

    protected function downloadLog($documentFile) {
        if ($this->editor) return;

        /** @var $downloadLog DownloadLog **/
        $downloadLog = $this->objectManager->get(DownloadLog::class);
        $downloadLog->setFrontendUser($this->currentUser);
        $downloadLog->setCrdate(new \DateTime());

        $documentFile->addDownloadLog($downloadLog);

        $this->documentFileRepository->update($documentFile);
        $this->persistenceManager->persistAll();
    }

}
