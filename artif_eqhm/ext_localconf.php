<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
	function () {

		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Artif.ArtifEqhm',
			'Dm',
			[
				'Document'  => 'list,newVersion,create,edit,new,upload,generatePDF,signVersion,update,compareWithBase,cloneFiles,makeCloneDocuments,notifications,pending,updateNewVersion,updateCompare',
				'DocumentAjax'  => 'getDocumentsByCategory,generatePDF',
				'Signature' => 'showVersionForSigning,signDocument'
			],
			// non-cacheable actions
			[
				'Document'  => 'list,newVersion,create,edit,new,upload,generatePDF,signVersion,update,compareWithBase,cloneFiles,makeCloneDocuments,notifications,pending,updateNewVersion,updateCompare',
                'DocumentAjax'  => 'getDocumentsByCategory,generatePDF',
				'Signature' => 'showVersionForSigning,signDocument'
			]
		);
		\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
			'Artif.ArtifEqhm',
			'Practice',
			[
				'Practice'  => 'edit, update',
			],
			// non-cacheable actions
			[
				'Practice'  => 'edit, update',
			]
		);
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Artif.ArtifEqhm',
            'Device',
            [
                'Device'  => 'list, show, new, create, edit, update, clone, makeClone',
            ],
            // non-cacheable actions
            [
                'Device'  => 'list, show, new, create, edit, update, clone, makeClone',
            ]
        );

		// wizards
		\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
			'mod {
                wizards.newContentElement.wizardItems.plugins {
                    elements {
                        dm {
                            icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('artif_eqhm') . 'Resources/Public/Icons/ext_icon.png
                            title = LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artif_eqhm_domain_model_dm
                            description = LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artif_eqhm_domain_model_dm.description
                            tt_content_defValues {
                                CType = list
                                list_type = artifeqhm_dm
                            }
                        }
                    }
                    show = *
                }
           }'
		);

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(\Artif\ArtifEqhm\Domain\TypeConverter\FileReferenceConverter::class);

        $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['t3lib/class.t3lib_tcemain.php']['processDatamapClass']['Artif.' . $_EXTKEY] = \Artif\ArtifEqhm\Hooks\DataHandlerHooks::class;

//        // Register cache frontend for proxy class generation
//        $GLOBALS['TYPO3_CONF_VARS']['SYS']['caching']['cacheConfigurations']['artif_eqhm'] = [
//            'frontend' => \TYPO3\CMS\Core\Cache\Frontend\PhpFrontend::class,
//            'groups' => [
//                'all',
//                'system',
//            ],
//            'options' => [
//                'defaultLifetime' => 0,
//            ]
//        ];
	}
);
