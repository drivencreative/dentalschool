<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Artif.ArtifEqhm',
            'Dm',
            'artif eqhm Document Management'
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Artif.ArtifEqhm',
            'Practice',
            'artif eqhm Practice Management'
        );
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Artif.ArtifEqhm',
            'Device',
            'artif eqhm Device Management'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('artif_eqhm', 'Configuration/TypoScript', 'artif eqhm');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifeqhm_domain_model_document', 'EXT:artif_eqhm/Resources/Private/Language/locallang_csh_tx_artifeqhm_domain_model_document.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifeqhm_domain_model_document');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifeqhm_domain_model_practice', 'EXT:artif_eqhm/Resources/Private/Language/locallang_csh_tx_artifeqhm_domain_model_practice.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifeqhm_domain_model_practice');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifeqhm_domain_model_signature', 'EXT:artif_eqhm/Resources/Private/Language/locallang_csh_tx_artifeqhm_domain_model_signature.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifeqhm_domain_model_signature');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifeqhm_domain_model_documentfile', 'EXT:artif_eqhm/Resources/Private/Language/locallang_csh_tx_artifeqhm_domain_model_documentfile.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifeqhm_domain_model_documentfile');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_artifeqhm_domain_model_terms', 'EXT:artif_eqhm/Resources/Private/Language/locallang_csh_tx_artifeqhm_domain_model_terms.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_artifeqhm_domain_model_terms');

//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
//            'artif_eqhm',
//            'tx_artifeqhm_domain_model_document'
//        );
//
//        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::makeCategorizable(
//            'artif_eqhm',
//            'tx_artifeqhm_domain_model_device'
//        );

    }
);
