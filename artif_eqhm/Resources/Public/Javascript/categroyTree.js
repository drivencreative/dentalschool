$(document).ready(function() {
	$('#tree-clone-files').on('changed.jstree', function (e, data) {
		var checked_ids = [];
		selectedElms = $('#tree-clone-files').jstree("get_checked",null,true);
		$.each(selectedElms, function(index, id) {
			if ($('#' + id).data('category')){
				checked_ids.push($('#' + id).data('category'));
			}
		});
		$("#selected-categories").val(checked_ids);
	});
	
});

$('#tree-clone-files')
	.on('select_node.jstree', function (e, data) {
		if (data.event) {
			data.instance.select_node(data.node.children_d);
		}
	})
	.on('deselect_node.jstree', function (e, data) {
		if (data.event) {
			data.instance.deselect_node(data.node.children_d);
		}
	})
	.jstree({
		"plugins" : [ "checkbox" ],
		'checkbox' : {
			"keep_selected_style" : false,
			"cascade" : "down",
			"three_state" : false
		},
		'expand_selected_onload' : true
	});


$('#tree-clone-files').jstree('select_all', true);
$('#tree').on('activate_node.jstree',function (event,data) {
	var dataUrlCategory = data.node.li_attr['data-url'];
	$('.tx-artif-eqhm').append('<div class="loader"><img src="typo3conf/ext/artif_eqhm/Resources/Public/Icons/Spinner.svg"></div>');
	$.ajax({
		url: dataUrlCategory,
	}).done(function(result) {
		$('#replaceAbleContent').replaceWith(result);
		$('.tx-artif-eqhm').find('.loader').remove();
	});
	
}).jstree();