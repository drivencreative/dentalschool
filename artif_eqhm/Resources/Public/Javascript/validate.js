$(document).ready(function () {
    validate('form.validate');
    function validate(el) {
        var validator = $(el).validate({
            errorPlacement: function() {},
            highlight: function(element) {
                $(element).addClass('f3-form-error');
            },
            unhighlight: function(element) {
                $(element).removeClass('f3-form-error');
            }
        });

        $('select').on('change', function () {
            if (validator) validator.element($(this));
        });
        $("input[type=file]").on('change', function () {
            if (validator) validator.element($(this));
        });
    }
});