<?php
defined('TYPO3_MODE') or die();
$additionalColumns = array(
    'categories' => array(
        'exclude' => 0,
        'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_category',
        'config' => array(
            'type' => 'inline',
            'foreign_table' => 'sys_category',
            'foreign_field' => 'parent',
        )
    ),
    'items' => [
        'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.items',
        'config' => [
            'type' => 'group',
            'internal_type' => 'db',
            'allowed' => '*',
            'foreign_table' => 'tx_artifeqhm_domain_model_document',
            'MM' => 'sys_category_record_mm',
            'MM_oppositeUsage' => [],
            'MM_match_fields' => array(
                'tablenames' => 'tx_artifeqhm_domain_model_document'
            ),
            'size' => 10,
            'fieldWizard' => [
                'recordsOverview' => [
                    'disabled' => true,
                ],
            ],
        ],
    ],
);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addTCAcolumns('sys_category', $additionalColumns);
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addToAllTCAtypes('sys_category', 'categories');
