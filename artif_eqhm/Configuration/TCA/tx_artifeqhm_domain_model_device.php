<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device',
        'label_alt' => 'uid',
        'label' => 'name',
        'label_alt_force' => true,
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'sortby' => 'sorting',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'type,name,manufacturer,ce_mark,serial_number,year_of_construction,space,number,notes',
        'iconfile' => 'EXT:artif_eqhm/Resources/Public/Icons/tx_artifeqhm_domain_model_device.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type,name,manufacturer,ce_mark,serial_number,year_of_construction,space,number,notes, categories, practice',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, type,name,manufacturer,ce_mark,serial_number,year_of_construction,space,number,notes,practice, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime, --div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category, categories'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_artifeqhm_domain_model_device',
                'foreign_table_where' => 'AND tx_artifeqhm_domain_model_device.pid=###CURRENT_PID### AND tx_artifeqhm_domain_model_device.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'type' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.type',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'name' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'manufacturer' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.manufacturer',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'ce_mark' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.ce_mark',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'serial_number' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.serial_number',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'year_of_construction' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.year_of_construction',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'space' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.space',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'number' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.number',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'notes' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_device.notes',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
        ],
        'categories' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.categories',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectTree',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.sorting ASC',
                'MM' => 'sys_category_record_mm',
                'MM_opposite_field' => 'items',
                'MM_match_fields' => [
                    'fieldname' => 'categories',
                    'tablenames' => 'tx_artifeqhm_domain_model_device'
                ],
                'maxitems' => 9999,
                'size' => 20,
                'treeConfig' => [
                    'parentField' => 'parent',
                    'appearance' => [
                        'expandAll' => 1,
                        'maxLevels' => 99,
                        'showHeader' => 1,
                    ],
                ],
            ],
        ],
        'practice' => [
            'exclude' => true,
            'label' => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.practice',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_artifeqhm_domain_model_practice',
                'minitems' => 1,
                'maxitems' => 1,
                'appearance' => [
                    'collapseAll' => 0,
                    'levelLinksPosition' => 'top',
                    'showSynchronizationLink' => 1,
                    'showPossibleLocalizationRecords' => 1,
                    'showAllLocalizationLink' => 1
                ],
            ],
        ],
    ],
];
