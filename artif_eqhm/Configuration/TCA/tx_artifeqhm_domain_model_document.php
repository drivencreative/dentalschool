<?php
return [
	'ctrl'      => [
		'title'                    => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document',
        'label_alt'                => 'uid',
		'label'                    => 'name',
        'label_alt_force'          => true,
		'tstamp'                   => 'tstamp',
		'crdate'                   => 'crdate',
		'cruser_id'                => 'cruser_id',
		'sortby'                   => 'sorting',
		'versioningWS'             => true,
		'languageField'            => 'sys_language_uid',
		'transOrigPointerField'    => 'l10n_parent',
		'transOrigDiffSourceField' => 'l10n_diffsource',
		'delete'                   => 'deleted',
		'enablecolumns'            => [
			'disabled'  => 'hidden',
			'starttime' => 'starttime',
			'endtime'   => 'endtime',
		],
		'searchFields'             => 'name,typ,based_on_document_version,document_files,practice,based_on',
		'iconfile'                 => 'EXT:artif_eqhm/Resources/Public/Icons/tx_artifeqhm_domain_model_document.gif'
	],
	'interface' => [
		'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, typ, based_on_document_version, document_files, practice, based_on, clone_of,notify,categories',
	],
	'types'     => [
		'1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, name, typ, based_on_document_version, document_files, practice, based_on, clone_of, notify,   --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime, --div--;LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.tabs.category, categories'],
	],
	'columns'   => [
		'sys_language_uid' => [
			'exclude' => true,
			'label'   => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
			'config'  => [
				'type'       => 'select',
				'renderType' => 'selectSingle',
				'special'    => 'languages',
				'items'      => [
					[
						'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
						-1,
						'flags-multiple'
					]
				],
				'default'    => 0,
			],
		],
		'l10n_parent'      => [
			'displayCond' => 'FIELD:sys_language_uid:>:0',
			'exclude'     => true,
			'label'       => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
			'config'      => [
				'type'                => 'select',
				'renderType'          => 'selectSingle',
				'items'               => [
					['', 0],
				],
				'foreign_table'       => 'tx_artifeqhm_domain_model_document',
				'foreign_table_where' => 'AND tx_artifeqhm_domain_model_document.pid=###CURRENT_PID### AND tx_artifeqhm_domain_model_document.sys_language_uid IN (-1,0)',
			],
		],
		'l10n_diffsource'  => [
			'config' => [
				'type' => 'passthrough',
			],
		],
		't3ver_label'      => [
			'label'  => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
			'config' => [
				'type' => 'input',
				'size' => 30,
				'max'  => 255,
			],
		],
		'hidden'           => [
			'exclude' => true,
			'label'   => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
			'config'  => [
				'type'  => 'check',
				'items' => [
					'1' => [
						'0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
					]
				],
			],
		],
		'starttime'        => [
			'exclude'   => true,
			'l10n_mode' => 'mergeIfNotBlank',
			'label'     => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
			'config'    => [
				'type'    => 'input',
				'size'    => 13,
				'eval'    => 'datetime',
				'default' => 0,
			],
		],
		'endtime'          => [
			'exclude'   => true,
			'l10n_mode' => 'mergeIfNotBlank',
			'label'     => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
			'config'    => [
				'type'    => 'input',
				'size'    => 13,
				'eval'    => 'datetime',
				'default' => 0,
				'range'   => [
					'upper' => mktime(0, 0, 0, 1, 1, 2038)
				],
			],
		],

		'name'                      => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.name',
			'config'  => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'typ'                       => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.typ',
			'config'  => [
				'type'       => 'select',
				'renderType' => 'selectSingle',
				'items'      => [
					['-- Label --', 0],
					['PDF', 'PortableDocumentFormat'],
					['RTE-Document', 'Table'],
					['Nicht editierbares PDF', 'NoneEditablePortableDocumentFormat'],
					['Link', 'Link'],
				],
				'size'       => 1,
				'maxitems'   => 1,
				'eval'       => ''
			],
		],
		'based_on_document_version' => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.based_on_document_version',
			'config'  => [
				'type' => 'input',
				'size' => 30,
				'eval' => 'trim'
			],
		],
		'document_files'            => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.document_files',
			'config'  => [
				'type'           => 'inline',
				'foreign_table'  => 'tx_artifeqhm_domain_model_documentfile',
				'foreign_field'  => 'document',
				'foreign_sortby' => 'version',
				'maxitems'       => 9999,
				'appearance'     => [
					'collapseAll'                     => 0,
					'levelLinksPosition'              => 'top',
					'showSynchronizationLink'         => 1,
					'showPossibleLocalizationRecords' => 1,
					'useSortable'                     => 1,
					'showAllLocalizationLink'         => 1
				],
			],

		],
		'practice'                  => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.practice',
			'config'  => [
				'type'          => 'select',
				'renderType'    => 'selectSingle',
				'foreign_table' => 'tx_artifeqhm_domain_model_practice',
				'minitems'      => 1,
				'maxitems'      => 1,
				'appearance'    => [
					'collapseAll'                     => 0,
					'levelLinksPosition'              => 'top',
					'showSynchronizationLink'         => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink'         => 1
				],
			],
		],
		'based_on'                  => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.based_on',
			'config'  => [
				'type'          => 'select',
				'renderType'    => 'selectSingle',
				'foreign_table' => 'tx_artifeqhm_domain_model_document',
				'minitems'      => 0,
				'maxitems'      => 1,
				'items'         => [
					['-- Label --', 0],
				],
				'appearance'    => [
					'collapseAll'                     => 0,
					'levelLinksPosition'              => 'top',
					'showSynchronizationLink'         => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink'         => 1
				],
			],
		],
		'clone_of'                  => [
			'exclude' => true,
			'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.clone_of',
			'config'  => [
				'type'          => 'select',
				'renderType'    => 'selectSingle',
				'foreign_table' => 'tx_artifeqhm_domain_model_document',
				'minitems'      => 0,
				'maxitems'      => 1,
				'items'         => [
					['-- Label --', 0],
				],
				'appearance'    => [
					'collapseAll'                     => 0,
					'levelLinksPosition'              => 'top',
					'showSynchronizationLink'         => 1,
					'showPossibleLocalizationRecords' => 1,
					'showAllLocalizationLink'         => 1
				],
			],
		],
        'notify' =>[
            'exclude' => 1,
            'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.notify',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ '', '' ],
                ],
            ],
        ],
        'generic' =>[
            'exclude' => 1,
            'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.generic',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ '', '' ],
                ],
            ],
        ],
        'is_hygiene_plan' =>[
            'exclude' => 1,
            'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.is_hygiene_plan',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ '', '' ],
                ],
            ],
        ],
        'is_device' =>[
            'exclude' => 1,
            'label'   => 'LLL:EXT:artif_eqhm/Resources/Private/Language/locallang_db.xlf:tx_artifeqhm_domain_model_document.is_device',
            'config' => [
                'type' => 'check',
                'items' => [
                    [ '', '' ],
                ],
            ],
        ],
        'categories' => [
            'exclude' => 1,
            'label' => 'LLL:EXT:lang/Resources/Private/Language/locallang_tca.xlf:sys_category.categories',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectTree',
                'foreign_table' => 'sys_category',
                'foreign_table_where' => ' AND sys_category.sys_language_uid IN (-1, 0) ORDER BY sys_category.sorting ASC',
                'MM' => 'sys_category_record_mm',
                'MM_opposite_field' => 'items',
                'MM_match_fields' => [
                    'fieldname' => 'categories',
                    'tablenames' => 'tx_artifeqhm_domain_model_document'
                ],
                'maxitems' => 9999,
                'size' => 20,
                'treeConfig' => [
                    'parentField' => 'parent',
                    'appearance' => [
                        'expandAll' => 1,
                        'maxLevels' => 99,
                        'showHeader' => 1,
                    ],
                ],
            ],
        ],
	],
];
