
plugin.tx_artifeqhm_dm {
    view {
        templateRootPaths.0 = EXT:artif_eqhm/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_artifeqhm_dm.view.templateRootPath}
        partialRootPaths.0 = EXT:artif_eqhm/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_artifeqhm_dm.view.partialRootPath}
        layoutRootPaths.0 = EXT:artif_eqhm/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_artifeqhm_dm.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_artifeqhm_dm.persistence.storagePid}
        recursive = 0
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
    settings {
        storagePid = {$plugin.tx_artifeqhm_dm.persistence.storagePid}
        #tempFilePath = /var/www/dentalschool/htdocs/typo3temp/temp
        tempFilePath = /srv/sftp/devdentalschool/htdocs/typo3temp/temp
        practiceEditUid = 420
        categories {
            hygieneplan = 6
            organigramm = 465
            QMHandbuch = 467
        }
        adminGroup = 3
        readOnlyGroup = 4
        pdf {

            qmhandbuchContent = user_upload/QM-Handbuch-Content.pdf
            cssForTable = Resources/Public/Css/DocumentTemplates/Table.css
            hygieneplanGeneratedPdfFromHtml = HygieneplanWkHtmlToPdf
            templateCss = Resources/Public/Css/DocumentTemplates/pdfDoc.css
            hygieneplanGeneratedPdfFromHtml {
                hygieneplanToAppend = HygieneplanTables
                templateTable = Templates/DocumentGenerator/HygieneplanTable.html
                templateCover = Templates/DocumentGenerator/Hygieneplan.html
            }
            organigrammGeneratedPdfFromHtml {
                template = Templates/DocumentGenerator/Organigramm.html
            }
            qmhandbuchGeneratedPdfFromHtml {
                template = Templates/DocumentGenerator/QMHandbuch.html
            }
        }
    }
}
plugin.tx_artifeqhm_practice {
    persistence {
        storagePid < plugin.tx_artifeqhm_dm.storagePid
    }
    settings < plugin.tx_artifeqhm_dm.settings
    settings {
        categories {
            hygieneplan = 6
        }
    }
    _CSS_DEFAULT_STYLE (
        .f3-form-error {
            border: 1px solid red;
            box-shadow: 0px 0px 10px red;
        }
    )
}
page{
    includeCSS {
        jstree = EXT:artif_eqhm/Resources/Public/Javascript/Libraries/vakata-jstree-a6a0d0d/dist/themes/default/style.min.css
        #pdf_doc = EXT:artif_eqhm/Resources/Public/Css/DocumentTemplates/pdfDoc.css
        artif_eqhm = EXT:artif_eqhm/Resources/Public/Css/DocumentTemplates/artif_eqhm.css
    }
    includeJSFooter{
        jquery.external = 1
        jquery = https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js
        jquery_validate = EXT:artif_eqhm/Resources/Public/Javascript/Libraries/jquery.validate.min.js
        validate = EXT:artif_eqhm/Resources/Public/Javascript/validate.js
        tinymce = EXT:artif_eqhm/Resources/Public/Javascript/Libraries/tinymce/js/tinymce/tinymce.min.js
        jstree = EXT:artif_eqhm/Resources/Public/Javascript/Libraries/vakata-jstree-a6a0d0d/dist/jstree.min.js
        categroyTree = EXT:artif_eqhm/Resources/Public/Javascript/categroyTree.js
        artif_eqhm = EXT:artif_eqhm/Resources/Public/Javascript/artif_eqhm.js
    }
}


// PAGE object for Ajax call:
ajaxselectlist_page = PAGE
ajaxselectlist_page {
    typeNum = 19852604

    config {
        disableAllHeaderCode = 1
        additionalHeaders = Content-type:application/html
        xhtml_cleaning = 0
        debug = 0
        no_cache = 1
        admPanel = 0
    }

    10 < tt_content.list.20.artifeqhm_dm
}

plugin.tx_artifeqhm_device {
    persistence {
        storagePid = {$plugin.tx_artifeqhm_device.persistence.storagePid}
        recursive = 0
    }
    view < plugin.tx_artifeqhm_dm.view
    features < plugin.tx_artifeqhm_dm.features
    mvc < plugin.tx_artifeqhm_dm.mvc
    settings < plugin.tx_artifeqhm_dm.settings
    settings {
        category = 31
    }
}

// PAGE object for Ajax call:
ajax_device = PAGE
ajax_device {
    typeNum = 26041985

    config {
        disableAllHeaderCode = 1
        additionalHeaders = Content-type:application/html
        xhtml_cleaning = 0
        debug = 0
        no_cache = 1
        admPanel = 0
    }

    10 < tt_content.list.20.artifeqhm_device
}