
plugin.tx_artifeqhm_dm {
    view {
        # cat=plugin.tx_artifeqhm_dm/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:artif_eqhm/Resources/Private/Templates/
        # cat=plugin.tx_artifeqhm_dm/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:artif_eqhm/Resources/Private/Partials/
        # cat=plugin.tx_artifeqhm_dm/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:artif_eqhm/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_artifeqhm_dm//a; type=string; label=Default storage PID
        storagePid = 411
    }
}

plugin.tx_artifeqhm_device {
    persistence {
    # cat=plugin.tx_artifeqhm_dm//a; type=string; label=Default storage PID
        storagePid = 427
    }
}